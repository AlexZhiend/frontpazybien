import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { PersonalmedicoService } from './../../../_service/personalmedico.service';
import { Personalmedico } from 'src/app/_model/personalmedico';
import { CategoriaexamenmedicoService } from './../../../_service/categoriaexamenmedico.service';
import { startWith, map } from 'rxjs/operators';
import { DetalleComprobante } from 'src/app/_model/detallecomprobante';
import { Categoriaexamenmedico } from 'src/app/_model/categoriaexamenmedico';
import { Observable } from 'rxjs';
import { ServiciomedicoService } from './../../../_service/serviciomedico.service';
import { Serviciomedico } from './../../../_model/serviciomedico';
import { FormGroup, FormControl } from '@angular/forms';
import { ComprobantePago } from './../../../_model/comprobantepago';
import { ComprobantepagoService } from './../../../_service/comprobantepago.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatAutocompleteSelectedEvent, MatPaginator, MatSort } from '@angular/material';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';

@Component({
  selector: 'app-notadecredito',
  templateUrl: './notadecredito.component.html',
  styleUrls: ['./notadecredito.component.css']
})
export class NotadecreditoComponent implements OnInit {

  ultimocomprobante= new ComprobantePago();
  numerocomprobante:string;
  fechaSeleccionada= new Date();
  fechaatencioneslab= new Date();
  notacredito= new ComprobantePago();
  form: FormGroup;
  
  // descuento:number;
  descuentos=[0,50,100];

  serviciomedicos: Serviciomedico[]=[];
  servic= new Serviciomedico();
  servicio: number;

  turnos:any = ['Mañana','Tarde'];
  turno: string;
  form3:FormGroup;
  total:number;
  test= new Serviciomedico();

  examen=new FormControl();
  filteredOptions1: Observable<Categoriaexamenmedico[]>;
  categoriaexamenesmedicos: Categoriaexamenmedico[]=[];
  categoriaexamenseleccionado= new Categoriaexamenmedico();
  detpordefecto: DetalleComprobante[]=[];

  dataSource:MatTableDataSource<DetalleComprobante>;
  detalles:DetalleComprobante[]=[];
  displayedColumns = ['cantidaddetalle', 'denominacionexamen','precioexamen','importedetalle','acciones'];

  ecosyrads: Personalmedico[]=[];
  personalselec=new Personalmedico();
  selectedexamen=new Categoriaexamenmedico();

  pacientes: Paciente[] = [];
  pacientess: Paciente[] = [];
  pacienteseleccionado: Paciente;
  displayedColumns3 = ['dnipaciente', 'nombresyapellidos','acciones'];
  dataSource3: MatTableDataSource<Paciente>;
  paci:Paciente;
  pacient:string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialogRef: MatDialogRef<NotadecreditoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ComprobantePago,
    public comprobantepagoService:ComprobantepagoService,
    public serviciomedicoService:ServiciomedicoService,
    private categoriaexamenmedicoService:CategoriaexamenmedicoService,
    private personalmedicoService:PersonalmedicoService,
    private pacienteservice:PacienteService
    ) { this.form3= new FormGroup({
      'cantidadcomprobante': new FormControl(1),
      'examen': new FormControl(null),
      'personal':new FormControl(null)
    }); }

  ngOnInit() {
    this.companterior();
    this.listarServiciomedico();
    this.listarcategoriaexamen();
    this.listarRadyEco();
    this.traerelNinguno();
    this.notacredito=this.data;
    this.notacredito.serieref=this.data.serie;
    this.notacredito.fechacompref=this.data.fechacomprobante;
    this.notacredito.totalref=this.data.total;
    this.notacredito.tiporef=this.data.tipo;
    this.notacredito.numeroref=this.data.numerorecibocomprobante;
    // this.descuento=this.data.descuento;
    this.notacredito.serviciomedico.idserviciomedico=this.data.serviciomedico.idserviciomedico;
    this.turno=this.data.turno;
    this.detalles =this.data.detallecomprobante;
    this.dataSource = new MatTableDataSource(this.detalles);
    this.notacredito;
    console.log(this.servic);




    this.filteredOptions1 = this.examen.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombrecategoriaexamenmedico),
      map(nombrecategoriaexamenmedico => nombrecategoriaexamenmedico ? this._filter1(nombrecategoriaexamenmedico) : this.categoriaexamenesmedicos.slice())
    );
  }


  companterior(){
    this.comprobantepagoService.listarComprobantePagoId().subscribe(data=>{
      this.ultimocomprobante=data;
      if(this.ultimocomprobante!=null){
      var numero= this.ultimocomprobante.numerorecibocomprobante + 1;
      }else{
        numero=1;
      }

      let numeronuevo=numero.toString();
      let cero = "0";
      for(var _i=0;_i < 7-numeronuevo.length; _i++){
        cero=cero+"0";
      }
      this.numerocomprobante=cero+numeronuevo;
      console.log(this.numerocomprobante)
    });
  }

  listarServiciomedico() {
    this.serviciomedicoService.listarServicioMedico().subscribe(data => {
      this.serviciomedicos = data;
    });
  }

  listarRadyEco(){
    this.personalmedicoService.listarSoloEcoYRad().subscribe(data=>{
      this.ecosyrads=data;
      let per = new Personalmedico();
      per.dnipersonalmedico="--";
      per.nombrespersonalmedico="--";
      per.areapersonalmedico="--";
      this.ecosyrads.push(per);
      console.log(this.ecosyrads)
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource3.filter = filterValue;
  }

  listarcategoriaexamen(){
    this.categoriaexamenmedicoService.listarCategoriaExamen().subscribe(data =>{
      this.categoriaexamenesmedicos=data;
    })
  }

  displayFn1(categoriaexamen?: Categoriaexamenmedico): string | undefined {
    return categoriaexamen ? categoriaexamen.nombrecategoriaexamenmedico : undefined;
  }

  private _filter1(nombrecategoriaexamenmedico: string): Categoriaexamenmedico[] {
    const filterValue = nombrecategoriaexamenmedico.toLowerCase();
    return this.categoriaexamenesmedicos.filter(categoria => categoria.nombrecategoriaexamenmedico.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
    this.categoriaexamenseleccionado=event.option.value;
  }

  agregar(){
    if (this.form3.valid===true) {
      if (this.notacredito.serviciomedico.idserviciomedico==1) {
        let det = new DetalleComprobante();
        this.personalselec=this.form3.value['personal'];
        if (this.personalselec!=null && this.personalselec.dnipersonalmedico!="--") {
          this.personalselec=this.form3.value['personal'];
          det.dnipersonalmedico=this.personalselec.dnipersonalmedico;
        }

        det.cantidad= this.form3.value['cantidadcomprobante'];
        det.categoriaexamenmedico= this.categoriaexamenseleccionado;
        det.importe=det.cantidad*det.categoriaexamenmedico.precio;
    
        this.detalles.push(det);
        console.log(this.detalles)
        this.dataSource = new MatTableDataSource(this.detalles);
        this.examen.setValue("");
      }else{
        this.comprobantepagoService.mensaje.next("Usted esta registrando un servicio medico, debe concluir con dicho registro o recargar la página");
      }

    }else{
      this.comprobantepagoService.mensaje.next("Falta algún dato requerido del examen médico");
    }
    
  }

  getTotalCost() {
    this.total=this.detalles.map(t => t.cantidad*t.categoriaexamenmedico.precio).reduce((acc, value) => acc + value, 0);
    return this.detalles.map(t => t.cantidad*t.categoriaexamenmedico.precio).reduce((acc, value) => acc + value, 0);
  }

  eliminar(transaction){
    var indice=this.detalles.indexOf(transaction);
    this.detalles.splice(indice, 1);
    this.dataSource = new MatTableDataSource(this.detalles);
  }



  operar(){
    // let paciente= new Paciente();
    let servicio2= new Serviciomedico();
    let servicio = new Serviciomedico();


    // servicio=this.form.value['idServiciomedicoSeleccionado'];

    // paciente.idpaciente=this.pacienteseleccionado.idpaciente;


    // if (this.form.value['idServiciomedicoSeleccionado']!=null) {
    //   servicio2.idserviciomedico=servicio.idserviciomedico;
    //   servicio2.precioserviciomedico=this.precioservicio.precioserviciomedico;
    // }else{
    //   servicio2=this.selected;
    //   servicio2.precioserviciomedico=0; 
    // }

    this.notacredito.idcomprobantepago=null;
    // this.comprobantepago.paciente = paciente;
    // this.comprobantepago.serviciomedico=servicio2;
    this.notacredito.numerorecibocomprobante=parseInt(this.numerocomprobante);

    this.notacredito.estado=1;
    console.log(this.notacredito.descuento);
    // if (this.form1.value['farmacia']!=0 ) {
    //   this.comprobantepago.cantidadfarmacia=this.form1.value['farmacia'];
    // }else{
    //   this.comprobantepago.cantidadfarmacia=this.cantidadtotalxdia;
    // }
    this.notacredito.turno;
    this.notacredito.tipo=2;
    this.notacredito.serie=2;
    this.notacredito.fechaatencion=this.notacredito.fechaatlab;
    // if(this.form.value['idServiciomedicoSeleccionado']!=null || this.llave==true){
    //   this.comprobantepago.detallecomprobante=this.detpordefecto;
    // }else{
    //   this.comprobantepago.detallecomprobante=this.detalles;
    // }

    for (let i = 0; i < this.detalles.length; i++) {
      this.detalles[i].iddetallecomprobante=null;
    }

    if (this.detalles.length==0) {
      this.notacredito.detallecomprobante=this.detpordefecto;
    } else {
      this.notacredito.detallecomprobante=this.detalles;
    }

    this.notacredito.serviciomedico;
    let totalfinal=(this.notacredito.serviciomedico.precioserviciomedico+this.total)*this.notacredito.descuento*(1/100);
    this.notacredito.total=(this.notacredito.cantidadfarmacia+this.notacredito.cantidadtopico+this.notacredito.cantidadotros+(this.notacredito.serviciomedico.precioserviciomedico+this.total)-totalfinal)*(-1);

    // let totalfinal=(this.notacredito.serviciomedico.precioserviciomedico+this.total)*this.notacredito.descuento*(1/100);
    // this.notacredito.total=this.notacredito.cantidadfarmacia+this.notacredito.cantidadtopico+this.notacredito.cantidadotros+(this.notacredito.serviciomedico.precioserviciomedico+this.total)-totalfinal;

    if (this.notacredito.paciente.idpaciente>=0){
      console.log(this.notacredito);
      this.comprobantepagoService.registrarComprobantePago(this.notacredito).subscribe(datas=>{
        this.comprobantepagoService.mensaje.next("Se realizó el registro con éxito");
        this.comprobantepagoService.reporteComprobanteU(parseInt(this.numerocomprobante)).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        });
      });
    }else{
        console.log(this.notacredito);
        this.comprobantepagoService.mensaje.next("Busque y seleccione al paciente antes de registrar");
    }    

  }

  cancelar(){
    this.comprobantepagoService.mensaje.next('Se canceló el procedimiento');
    window.location.reload();
  }


  traerelNinguno(){
    this.categoriaexamenmedicoService.listarCategoriaexamenmedicoId(39).subscribe(data =>{
      this.selectedexamen=data;

      let det = new DetalleComprobante();

      det.cantidad= this.form3.value['cantidadcomprobante'];
      det.categoriaexamenmedico = this.selectedexamen;
      det.importe=det.cantidad*det.categoriaexamenmedico.precio;

      this.detpordefecto.push(det);
      console.log(this.detpordefecto);
    })
  }

  pdf(){
    this.comprobantepagoService.reporteComprobanteU(parseInt(this.numerocomprobante)).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    });
  }


  generar(){
    this.serviciomedicoService.listarServiciomedicoId(this.notacredito.serviciomedico.idserviciomedico).subscribe(data=>{
      this.test=data;
      this.notacredito.serviciomedico=this.test;
      console.log(data);
    })
  }

  consultarpac(){
    if (this.pacient!='') {
      this.pacienteservice.buscarPorNombre(this.pacient).subscribe(data=>{
        if (data.length==0) {
          this.pacienteservice.mensaje.next('No se encontraron coincidencias');
        }
        this.pacientess = data;
        this.dataSource3 = new MatTableDataSource(this.pacientess);
        this.dataSource3.paginator = this.paginator;
        this.dataSource3.sort = this.sort;
      })
    }else{
      this.pacienteservice.mensaje.next('Ingrese un indicio del nombre a buscar')
    }
  }

  addpac(paci:Paciente){
    this.notacredito.paciente=paci;
  }

}
