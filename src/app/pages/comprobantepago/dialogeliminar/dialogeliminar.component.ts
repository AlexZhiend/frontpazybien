import { Recaudo } from './../../../_model/recaudo';
import { RecaudoService } from './../../../_service/recaudo.service';
import { ComprobantepagoService } from './../../../_service/comprobantepago.service';
import { ComprobantePago } from './../../../_model/comprobantepago';
import { MatDialog, MatTableDataSource, MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-dialogeliminar',
  templateUrl: './dialogeliminar.component.html',
  styleUrls: ['./dialogeliminar.component.css']
})
export class DialogeliminarComponent implements OnInit {
  comprobantes:ComprobantePago[]=[];
  numerocomprobante:number;
  dataSource:MatTableDataSource<ComprobantePago>;
  displayedColumns = ['numerocomprobante', 'paciente','fecha','importedetalle','estado','acciones'];
  comprobanteseleccioando:ComprobantePago;
  total:number;
  cp:any='';
  recaudo= new Recaudo();
  numeros:string='';

  constructor(public dialog:MatDialog,
    public dialogRef: MatDialogRef<DialogeliminarComponent>,
              private comprobantepagoService:ComprobantepagoService,
              private recaudoService:RecaudoService) { }

  ngOnInit() {
      let fecha = new Date();
      let s1 = moment(fecha).format("DD-MM-YYYY");
      console.log(s1)
      this.recaudoService.BuscarRecaudoXFecha('03-01-2020').subscribe(data=>{
        this.recaudo=data;
      })
  }

  consultarrecibo(){
    this.comprobantepagoService.listarComprobantePagoxnumero(this.numerocomprobante).subscribe(data=>{
      this.comprobanteseleccioando=data;
      this.comprobantes.push(this.comprobanteseleccioando);
      this.dataSource = new MatTableDataSource(this.comprobantes);
      this.comprobanteseleccioando=null;
      this.numerocomprobante=null;
    });
  }

  getTotalCost() {
    this.total=this.comprobantes.map(t => t.total).reduce((acc, value) => acc + value, 0);
    return this.comprobantes.map(t => t.total).reduce((acc, value) => acc + value, 0);
  }

  eliminar(transaction){
    var indice=this.comprobantes.indexOf(transaction);
    this.comprobantes.splice(indice, 1);
    this.dataSource = new MatTableDataSource(this.comprobantes);
  }

  close(){
    this.dialogRef.close();
    this.comprobantepagoService.mensaje.next('Se canceló el procedimiento');
  }

  eliminarrecibo(){
    if (this.comprobantes.length!=0) {  
      for (let i = 0; i < this.comprobantes.length; i++) {
        this.comprobantes[i].estado=0;
        this.numeros += this.comprobantes[i].numerorecibocomprobante.toString()+' , ';
        console.log(this.numeros); 
        this.comprobantepagoService.modificarcomprobante(this.comprobantes[i]).subscribe(data=>{
        })
      }
      this.recaudo.descripcion=this.numeros;
      this.recaudo.montoanulado=this.total;
      this.recaudoService.modificarRecaudo(this.recaudo).subscribe(data=>{});
      this.comprobantepagoService.mensaje.next('Se eliminaron los recibos correctamente');
    }else{
      this.comprobantepagoService.mensaje.next('No se pudo realizar la operación, faltan datos');
    }
  }

  pdf(transaction:ComprobantePago){
    this.comprobantepagoService.reporteComprobanteU(transaction.numerorecibocomprobante).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    });
  }


}
