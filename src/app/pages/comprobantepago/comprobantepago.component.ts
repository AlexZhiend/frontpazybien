import { NotadecreditoComponent } from './notadecredito/notadecredito.component';
import { PersonalmedicoService } from './../../_service/personalmedico.service';
import { CajaRecaudo } from './../../_model/totalrecaudo';
import { RecaudoService } from './../../_service/recaudo.service';
import { DialogeliminarComponent } from './dialogeliminar/dialogeliminar.component';
import { DialogcomprobanteComponent } from './dialogcomprobante/dialogcomprobante.component';
import { CategoriaexamenmedicoService } from './../../_service/categoriaexamenmedico.service';
import { Categoriaexamenmedico } from 'src/app/_model/categoriaexamenmedico';
import { OrdenfarmaciaService } from './../../_service/ordenfarmacia.service';
import { TotalxDiaFarmacia } from './../../_model/totalfarmacia';
import { ExamenmedicoService } from './../../_service/examenmedico.service';
import { Paciente } from './../../_model/paciente';
import { startWith, map } from 'rxjs/operators';
import { Serviciomedico } from './../../_model/serviciomedico';
import { MatSnackBar, MatSelectChange, MatOption, MatAutocompleteSelectedEvent, MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ServiciomedicoService } from './../../_service/serviciomedico.service';
import { PacienteService } from './../../_service/paciente.service';
import { ComprobantepagoService } from './../../_service/comprobantepago.service';
import { ComprobantePago } from './../../_model/comprobantepago';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ExamenMedico } from 'src/app/_model/examenmedico';
import { DetalleComprobante } from 'src/app/_model/detallecomprobante';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';
import { Recaudo } from 'src/app/_model/recaudo';
import { Personalmedico } from 'src/app/_model/personalmedico';


@Component({
  selector: 'app-comprobantepago',
  templateUrl: './comprobantepago.component.html',
  styleUrls: ['./comprobantepago.component.css']
})
export class ComprobantepagoComponent implements OnInit {
  comprobantepago: ComprobantePago;
  displayedColumns = ['cantidaddetalle', 'denominacionexamen','precioexamen','importedetalle','acciones'];

  serviciomedicos: Serviciomedico[]=[]; 
  pacientes: Paciente[] = [];
  pacientess: Paciente[] = [];
  categoriaexamenesmedicos: Categoriaexamenmedico[]=[];

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  form: FormGroup;
  form1: FormGroup;
  form2: FormGroup;
  form3:FormGroup;

  examen=new FormControl();

  numerocomprobante:string;
  filteredOptions1: Observable<Categoriaexamenmedico[]>;


  pacienteseleccionado: Paciente;
  dataSource:MatTableDataSource<DetalleComprobante>;
  detalles:DetalleComprobante[]=[];
  ultimocomprobante=new ComprobantePago();
  idpacienteseleccionado:number;

  precioservicio=new Serviciomedico();
  categoriaexamenseleccionado= new Categoriaexamenmedico();

  fechaSeleccionada1: Date = null;
  fechaSeleccionada2: Date = null;
  fechaSeleccionada3: Date = null;
  fechaSeleccionada4: Date = null;
  fechaatencioneslab: Date = new Date();
  fechaDetallada: Date = new Date();
  fecharecinicio: Date = new Date();
  fecharecfin: Date = new Date();
  
  cp:any='';
  vent:any='';
  at:any='';
  cpunit:any='';


  comprobanteseleccionado= new ComprobantePago();
  paciente1:string;
  comprobantepagos: ComprobantePago[]=[];

  totalxdiaFarmacia: TotalxDiaFarmacia;
  cantidadtotalxdia:number=0;


  turnos:any = ['Mañana','Tarde'];

  selected=new Serviciomedico();
  selectedexamen=new Categoriaexamenmedico();
  detpordefecto: DetalleComprobante[]=[];
  llave=false;
  total:number;
  fechaatencion:Date;
  fechaingreso=new Date();

  descuentos=[0,50,100];
  descuento:number=0;

  displayedColumns3 = ['dnipaciente', 'nombresyapellidos','acciones'];
  dataSource3: MatTableDataSource<Paciente>;
  paci:Paciente;
  montorecaudado:number;
  recaudo= new Recaudo();

  ecosyrads: Personalmedico[]=[];
  personalselec=new Personalmedico();
  recib:number;
  pacient:string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private comprobantepagoService:ComprobantepagoService,
              private pacienteservice:PacienteService,
              private serviciomedicoService:ServiciomedicoService,
              public snackBar:MatSnackBar,
              private categoriaexamenmedicoService:CategoriaexamenmedicoService,
              private ordenfarmaciaService:OrdenfarmaciaService,
              private recaudoService:RecaudoService,
              public dialog:MatDialog,
              private personalmedicoService:PersonalmedicoService
              ) { 
                this.comprobantepago=new ComprobantePago();
                this.form= new FormGroup({
                  'fechaseleccionada': new FormControl(null),
                  'hclSeleccionado': new FormControl(null),
                  'idServiciomedicoSeleccionado': new FormControl(),
                  'otros':new FormControl(0.0),
                  'fechaAtencion':new FormControl(this.maxFecha),
                  'turno':new FormControl(null),
                });
                this.form1= new FormGroup({
                  'farmacia': new FormControl(0.0),
                });
                this.form2= new FormGroup({
                  'topico': new FormControl(0.0),
                });
                this.form3= new FormGroup({
                  'cantidadcomprobante': new FormControl(1),
                  'examen': new FormControl(null),
                  'personal':new FormControl(null)
                });
                this.detalles=[];
                this.pacienteseleccionado=new Paciente();
              }

  ngOnInit() {
    this.listarPaciente();
    this.listarComprobante();
    this.listarServiciomedico();
    this.listarcategoriaexamen();
    this.companterior();
    this.traerelNinguno();
    this.listarRadyEco();
    
    this.selected.idserviciomedico=1;

    this.comprobantepagoService.mensaje.subscribe(data=>{
    this.snackBar.open(data, null, { duration: 3000 });
    })

    // this.filteredOptions = this.paciente.valueChanges.pipe(
    //     startWith(''),
    //     map(value => typeof value === 'string' ? value : value.nombresyapellidos),
    //     map(nombresyapellidos => nombresyapellidos ? this._filter(nombresyapellidos) : this.pacientes.slice())
    //   );
    
      this.filteredOptions1 = this.examen.valueChanges.pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombrecategoriaexamenmedico),
        map(nombrecategoriaexamenmedico => nombrecategoriaexamenmedico ? this._filter1(nombrecategoriaexamenmedico) : this.categoriaexamenesmedicos.slice())
      );

  }

  companterior(){
    this.comprobantepagoService.listarComprobantePagoId().subscribe(data=>{
      this.ultimocomprobante=data;
      if(this.ultimocomprobante!=null){
      var numero= this.ultimocomprobante.numerorecibocomprobante + 1;
      }else{
        numero=1;
      }

      let numeronuevo=numero.toString();
      let cero = "0";
      for(var _i=0;_i < 7-numeronuevo.length; _i++){
        cero=cero+"0";
      }
      this.numerocomprobante=cero+numeronuevo;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource3.filter = filterValue;
  }

  listarPaciente() {
    this.pacienteservice.listarpaciente().subscribe(data => {
      this.pacientes = data;
    });
  }

  listarServiciomedico() {
    this.serviciomedicoService.listarServicioMedico().subscribe(data => {
      this.serviciomedicos = data;
    });
  }

  listarcategoriaexamen(){
    this.categoriaexamenmedicoService.listarCategoriaExamen().subscribe(data =>{
      this.categoriaexamenesmedicos=data;
    })
  }

  listarComprobante(){
    this.comprobantepagoService.listarComprobantesVigentes().subscribe(data=>{
      this.comprobantepagos=data;
    })
  }

  listarRadyEco(){
    this.personalmedicoService.listarSoloEcoYRad().subscribe(data=>{
      this.ecosyrads=data;
      let per = new Personalmedico();
      per.dnipersonalmedico="--";
      per.nombrespersonalmedico="--";
      per.areapersonalmedico="--";
      this.ecosyrads.push(per);
      console.log(this.ecosyrads)
    })
  }

  
  // displayFn(paciente?: Paciente): string | undefined {
  //   return paciente ? paciente.nombresyapellidos : undefined;
  // }

  displayFn1(categoriaexamen?: Categoriaexamenmedico): string | undefined {
    return categoriaexamen ? categoriaexamen.nombrecategoriaexamenmedico : undefined;
  }



  // private _filter(nombresyapellidos: string): Paciente[] {
  //   const filterValue = nombresyapellidos.toLowerCase();
  //   return this.pacientes.filter(paciente => paciente.nombresyapellidos.toLowerCase().indexOf(filterValue) === 0);
  // }


  private _filter1(nombrecategoriaexamenmedico: string): Categoriaexamenmedico[] {
    const filterValue = nombrecategoriaexamenmedico.toLowerCase();
    return this.categoriaexamenesmedicos.filter(categoria => categoria.nombrecategoriaexamenmedico.toLowerCase().indexOf(filterValue) === 0);
  }




  // onSelectionChanged(event: MatAutocompleteSelectedEvent) {
  //   this.pacienteseleccionado=event.option.value;
  //   this.idpacienteseleccionado=this.pacienteseleccionado.idpaciente
  // }

  onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
    this.categoriaexamenseleccionado=event.option.value;
  }


  consultar( ) {
    if (this.recib!=0) {
      this.comprobantepagoService.BuscarComprobanteporNum(this.recib).subscribe(data =>{
        this.comprobanteseleccionado = data;
        this.paciente1=this.comprobanteseleccionado.paciente.nombresyapellidos;
        this.comprobantepagoService.reporteComprobanteU(this.comprobanteseleccionado.numerorecibocomprobante).subscribe(data=>{
          this.cpunit=data;
        })
      })
    }else{
      this.comprobantepagoService.mensaje.next('Ingrese el número del recibo a consultar')
    }   
  }

  servicio(event: MatSelectChange) {
    this.precioservicio=event.source.value;
    console.log(this.precioservicio)
  }

  desc(event: MatSelectChange) {
    this.descuento=event.source.value;
    console.log(this.descuento)
  }

  agregar(){
    if (this.form3.valid===true) {
      if (this.form.value['idServiciomedicoSeleccionado']==null) {
        let det = new DetalleComprobante();
        this.personalselec=this.form3.value['personal'];
        if (this.personalselec!=null && this.personalselec.dnipersonalmedico!="--") {
          this.personalselec=this.form3.value['personal'];
          det.dnipersonalmedico=this.personalselec.dnipersonalmedico;
        }


        det.cantidad= this.form3.value['cantidadcomprobante'];
        det.categoriaexamenmedico= this.categoriaexamenseleccionado;
        det.importe=det.cantidad*det.categoriaexamenmedico.precio;
    
        this.detalles.push(det);
        console.log(this.detalles)
        this.dataSource = new MatTableDataSource(this.detalles);
        this.examen.setValue("");
      }else{
        this.comprobantepagoService.mensaje.next("Usted esta registrando un servicio medico, debe concluir con dicho registro o recargar la página");
      }

    }else{
      this.snackBar.open("Falta algún dato requerido del examen médico");
    }
    
  }

  getTotalCost() {
    this.total=this.detalles.map(t => t.importe).reduce((acc, value) => acc + value, 0);
    return this.detalles.map(t => t.importe).reduce((acc, value) => acc + value, 0);
  }

  operar(){
    let paciente= new Paciente();
    let servicio2= new Serviciomedico();
    let servicio = new Serviciomedico();

    servicio=this.form.value['idServiciomedicoSeleccionado'];

    paciente.idpaciente=this.pacienteseleccionado.idpaciente;


    if (this.form.value['idServiciomedicoSeleccionado']!=null) {
      servicio2.idserviciomedico=servicio.idserviciomedico;
      servicio2.precioserviciomedico=this.precioservicio.precioserviciomedico;
    }else{
      servicio2=this.selected;
      servicio2.precioserviciomedico=0; 
    }

    this.comprobantepago.fechacomprobante=this.fechaSeleccionada;
    this.comprobantepago.paciente = paciente;
    this.comprobantepago.serviciomedico=servicio2;
    this.comprobantepago.numerorecibocomprobante=parseInt(this.numerocomprobante);

    this.comprobantepago.cantidadotros=this.form.value['otros'];
    this.comprobantepago.cantidadtopico=this.form2.value['topico'];
    this.comprobantepago.estado=1;
    this.comprobantepago.descuento=this.descuento;
    this.comprobantepago.fechaatlab=this.fechaatencioneslab;
    this.comprobantepago.serie=2;
    this.comprobantepago.tipo=1;

    if (this.form1.value['farmacia']!=0 ) {
      this.comprobantepago.cantidadfarmacia=this.form1.value['farmacia'];
    }else{
      this.comprobantepago.cantidadfarmacia=this.cantidadtotalxdia;
    }
    this.comprobantepago.turno=this.form.value['turno'];
    this.comprobantepago.fechaatencion=this.fechaatencioneslab;

    if(this.form.value['idServiciomedicoSeleccionado']!=null || this.llave==true){
      this.comprobantepago.detallecomprobante=this.detpordefecto;
    }else{
      this.comprobantepago.detallecomprobante=this.detalles;
    }

    let totalfinal=(servicio2.precioserviciomedico+this.total)*this.descuento*(1/100);
    this.comprobantepago.total=this.comprobantepago.cantidadfarmacia+this.comprobantepago.cantidadtopico+this.comprobantepago.cantidadotros+(servicio2.precioserviciomedico+this.total)-totalfinal;

    if (this.comprobantepago.paciente.idpaciente>=0){
      console.log(this.pacienteseleccionado.idpaciente);
      this.comprobantepagoService.registrarComprobantePago(this.comprobantepago).subscribe(datas=>{
        this.comprobantepagoService.mensaje.next("Se realizó el registro con éxito");
        this.comprobantepagoService.reporteComprobanteU(parseInt(this.numerocomprobante)).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        });
      });
    }else{
        console.log(this.comprobantepago.paciente.idpaciente);
        this.comprobantepagoService.mensaje.next("Busque y seleccione al paciente antes de registrar");
    }    

  }

  eliminar(transaction){
    var indice=this.detalles.indexOf(transaction);
    this.detalles.splice(indice, 1);
    this.dataSource = new MatTableDataSource(this.detalles);
    
  }

  cancelar(){
    this.form.reset();
    this.comprobantepagoService.mensaje.next('Se canceló el procedimiento');
    window.location.reload();
  }

  pdf(){
    this.comprobantepagoService.reporteComprobanteU(parseInt(this.numerocomprobante)).subscribe(data=>{
      this.cp=data;
    });
  }

  pdfCaja(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteCaja(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfAtencion(){
    if (this.fechaSeleccionada3!=null) {
        let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteAtencion(s1).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
    }
  }

  eliminarCP(){
    if (this.comprobanteseleccionado!=null && this.comprobanteseleccionado.idcomprobantepago>0) {
      this.comprobanteseleccionado.estado=0;
      this.comprobantepagoService.modificarcomprobante(this.comprobanteseleccionado).subscribe(data=>{
        this.comprobantepagoService.mensaje.next('El recibo se eliminó correctamente');
        this.comprobanteseleccionado=null;
      });
      this.recib=0;
      this.paciente1=null;
      this.comprobanteseleccionado=null;
    }else{
      this.comprobantepagoService.mensaje.next('Ingrese el número del recibo a eliminar')
    }

  }

  registrarFT(){
    let paciente= new Paciente();
    let servicio2= new Serviciomedico();
    let servicio = new Serviciomedico();

    servicio=this.form.value['idServiciomedicoSeleccionado'];
  }

  totalxdiafarmacias(){
    let s1 = moment(this.fechaSeleccionada).format("DD-MM-YYYY");
    this.ordenfarmaciaService.TotalFarmaciaxdia(s1).subscribe(data=>{
      this.totalxdiaFarmacia=data;
      if (this.totalxdiaFarmacia!=null) {
        this.cantidadtotalxdia=this.totalxdiaFarmacia.totalxdia;
      }
      else{
        this.comprobantepagoService.mensaje.next('No se encontraron registros')
      }
    })
  }

  traerelNinguno(){
    this.categoriaexamenmedicoService.listarCategoriaexamenmedicoId(39).subscribe(data =>{
      this.selectedexamen=data;

      let det = new DetalleComprobante();

      det.cantidad= this.form3.value['cantidadcomprobante'];
      det.categoriaexamenmedico = this.selectedexamen;
      det.importe=det.cantidad*det.categoriaexamenmedico.precio;

      this.detpordefecto.push(det);
      console.log(this.detpordefecto);
    })
  }

  addfar(){
    this.llave=true;
    this.comprobantepago.detallecomprobante=this.detpordefecto;
    console.log(this.comprobantepago);
  }

  addtop(){
    this.llave=true;
    this.comprobantepago.detallecomprobante=this.detpordefecto;
    console.log(this.comprobantepago);
  }

  addpac(paci:Paciente){
    this.pacienteseleccionado=paci;
  }

  opendialog(){
    let dialogRef1 = this.dialog.open(DialogcomprobanteComponent, {
      width: '500px',   
      disableClose: false
    });
  }

  pdfDetallado(){
    if (this.fechaDetallada!=null) {
      let s1 = moment(this.fechaDetallada).format("DD-MM-YYYY");
      this.comprobantepagoService.reporteCajaDetallado(s1).subscribe(data=>{
        var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
        var fileURL = window.URL.createObjectURL(data2);
        window.open(fileURL,"_blank");
      })
  }else{
    this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
  }
  }


  pdfCajaXLSX(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteCajaXLSX(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"ReporteCajaGeneral.xlsx");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfAtencionXLSX(){
    if (this.fechaSeleccionada3!=null) {
        let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteAtencionXLSX(s1).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"Atenciones-"+s1+".xlsx");
        })
    }else{
      this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
    }
  }

  pdfDetalladoXLSX(){
    if (this.fechaDetallada!=null) {
      let s1 = moment(this.fechaDetallada).format("DD-MM-YYYY");
      this.comprobantepagoService.reporteCajaDetalladoXLSX(s1).subscribe(data=>{
        var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
        FileSaver.saveAs(data2,"RDetalladoCaja-"+s1+".xlsx");
      })
  }else{
    this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
  }
  }

  openeliminar(){
    let dialogRef = this.dialog.open(DialogeliminarComponent, {
      width: '80%',   
      disableClose: true
    });
  }


  verificaring(){
      let s1 = moment(this.fechaingreso).format("DD-MM-YYYY");
      this.recaudoService.listarRecaudoXdia(s1).subscribe(data=>{
        if (data==null) {
          this.comprobantepagoService.mensaje.next('No se encontraron ingresos');
        }else{
          this.montorecaudado=data.totalxdia;
          console.log(this.montorecaudado)
        }
      })
  }

  registraring(){
      this.recaudo.monto=this.montorecaudado;
      this.recaudo.fecha=this.fechaingreso;
      console.log(this.recaudo)
      if (this.recaudo.monto!=undefined) {
        this.recaudoService.registrarRecaudo(this.recaudo).subscribe(data=>{
          if (data!=null) {
            this.comprobantepagoService.mensaje.next('Se registró correctamente');
          }
        });
      }else{
        this.comprobantepagoService.mensaje.next('Debe verificar el monto de hoy');
      }

  }

  recaudoreport(){
    if (this.fecharecinicio!=null && this.fecharecfin!=null 
      && this.fecharecinicio<=this.fecharecfin) {
        let s1 = moment(this.fecharecinicio).format("DD-MM-YYYY");
        let s2 = moment(this.fecharecfin).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteNCCajaXLSX(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"Reporte.xlsx");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  recaudoreportpdf(){
    if (this.fecharecinicio!=null && this.fecharecfin!=null 
      && this.fecharecinicio<=this.fecharecfin) {
        let s1 = moment(this.fecharecinicio).format("DD-MM-YYYY");
        let s2 = moment(this.fecharecfin).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteNCCajaPDF(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }
  
  consultarpac(){
    if (this.pacient!='') {
      this.pacienteservice.buscarPorNombre(this.pacient).subscribe(data=>{
        if (data.length==0) {
          this.pacienteservice.mensaje.next('No se encontraron coincidencias');
        }
        this.pacientess = data;
        this.dataSource3 = new MatTableDataSource(this.pacientess);
        this.dataSource3.paginator = this.paginator;
        this.dataSource3.sort = this.sort;
      })
    }else{
      this.pacienteservice.mensaje.next('Ingrese un indicio del nombre a buscar')
    }
  }


  generarFormato(){
    let s1 = moment(this.fechaingreso).format("DD-MM-YYYY");
    this.recaudoService.formatoCaja(s1,s1).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    })
  }

  notacredito(){
    if (this.comprobanteseleccionado!=null && this.comprobanteseleccionado.idcomprobantepago>0) {
      let pro = this.comprobanteseleccionado;
      let dialogRef = this.dialog.open(NotadecreditoComponent, {
        width: '95%',   
        disableClose: false,
        data: pro
      });
    } else {
      this.comprobantepagoService.mensaje.next("Busque el comprobante antes de generar la nota de crédito")
    }

  }

}
