import { AtencionLabReport } from './../../../_model/atencionlabreport';
import { ComprobantepagoService } from './../../../_service/comprobantepago.service';
import { ComprobantePago } from 'src/app/_model/comprobantepago';
import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-dialogcomprobante',
  templateUrl: './dialogcomprobante.component.html',
  styleUrls: ['./dialogcomprobante.component.css']
})
export class DialogcomprobanteComponent implements OnInit {

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  atenciones:AtencionLabReport[]=[];
  atencion:number;

  constructor(public dialog:MatDialog,
              private comprobantepagoService:ComprobantepagoService ) { }

  ngOnInit() {
  }

  listar(){
    let s1 = moment(this.fechaSeleccionada).format("DD-MM-YYYY");
    this.comprobantepagoService.repAtencionesLab(s1).subscribe(data => {
      this.atenciones = data;
      this.atencion=this.atenciones.length;
    }); 
    }

}
