import { NotacreditoOrdenComponent } from './notacredito-orden/notacredito-orden.component';
import { OrdenFarmacia } from './../../_model/orden';
import { map, startWith } from 'rxjs/operators';

import { Detalleofvista } from './../../_model/detalleofvista';
import { DetalleExamen } from 'src/app/_model/detalleeg';
import { Producto } from './../../_model/producto';
import { OrdenfarmaciaService } from './../../_service/ordenfarmacia.service';
import { MatSnackBar, MatTableDataSource, MatSelectChange, MatOption, MatAutocompleteSelectedEvent, MatDialog } from '@angular/material';
import { DetalleOF } from './../../_model/detalleof';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Component, OnInit } from '@angular/core';

import { ProductoService } from 'src/app/_service/producto.service';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';


const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXCEL_EXT = '.xlsx';

@Component({
  selector: 'app-orden',
  templateUrl: './orden.component.html',
  styleUrls: ['./orden.component.css']
})
export class OrdenComponent implements OnInit {

  displayedColumns: string[] = ['cantidaddetalle', 'denominacionproducto','presentacion','pventaproducto','importedetalle', 'acciones'];
  productos: Producto[] = [];

  productoseleccionado: Producto;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  ordenFarmacia:OrdenFarmacia;

  producto=new FormControl();
  filteredOptions: Observable<Producto[]>;

  total:number;

  form:FormGroup;
  form1:FormGroup;

  detalle:DetalleOF;
  detalles:DetalleOF[]=[];

  dataSource: MatTableDataSource<DetalleOF>;


  ultimaorden:OrdenFarmacia;
  numeroorden:string;
  productostock:Producto;
  stock:number;
  nombres:number[];
  a:Producto;
  cantidades:number[];
  stockreal:number;
  idproductoseleccionado: number;
  or:any='';
  vent:any='';
  cpunit:any='';

  descuentos=[0,50,100];
  descuento:number=0;

  fechaDetallada: Date = new Date();

  comprobante = new FormControl();
  filteredOptions2: Observable<OrdenFarmacia[]>;
  comprobanteseleccionado= new OrdenFarmacia();
  ordenes: OrdenFarmacia[]=[];

  numerorecib:number;
  ordenseleccionada= new OrdenFarmacia();

  fechainicio: Date = new Date();
  fechafin: Date = new Date();

  constructor(private ordenfarmaciaService:OrdenfarmaciaService,
    private productoService:ProductoService,public dialog:MatDialog,
    public snackBar:MatSnackBar) {
      this.cantidades=[];
      this.nombres=[];
      this.detalle = new DetalleOF();
      this.ordenFarmacia=new OrdenFarmacia();
      this.ordenFarmacia.detalleordenF=new DetalleOF();
      this.form= new FormGroup({
        'numeroorden': new FormControl({value:0 , disabled:true}, Validators.required),
        'rucordenfarmacia': new FormControl(0),
        'fechaseleccionada': new FormControl(new Date),
        'consumidorordenfarmacia': new FormControl('')
      });
      this.form1=new FormGroup({
        'cantidadfarmacia':new FormControl(1),
        'stock': new FormControl({value:0 , disabled:true}, Validators.required),
      });
      this.productoseleccionado= new Producto();
     }

  ngOnInit() {
    this.listarproducto();
    this.companterior();
    this.listarComprobante();

    this.ordenfarmaciaService.mensaje.subscribe(data=>{
      this.snackBar.open(data, null, { duration: 4000 });
      });

      this.filteredOptions = this.producto.valueChanges.pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombreproducto),
        map(nombreproducto => nombreproducto ? this._filter(nombreproducto) : this.productos.slice())
      );

      this.filteredOptions2 = this.comprobante.valueChanges.pipe(
        startWith(),
        map(value => typeof value === 'string' ? value : value.numeroorden),
        map(numeroorden => numeroorden ? this._filter2(numeroorden) : this.ordenes.slice())
      );
  }

  listarproducto(){
    this.productoService.listarproductos().subscribe(data => {
      this.productos=data;
    })
  }

  
  listarComprobante(){
    this.ordenfarmaciaService.listarOrdenFarmacia().subscribe(data=>{
      this.ordenes=data;
    })
  }

  displayFn(producto?: Producto): string | undefined {
    return producto ? producto.nombreproducto : undefined;
  }

  private _filter(nombreproducto: string): Producto[] {
    const filterValue = nombreproducto.toLowerCase();
    return this.productos.filter(producto => producto.nombreproducto.toLowerCase().indexOf(filterValue) === 0);
  }

    
  displayFn2(orden?: OrdenFarmacia): number | undefined {
    return orden ? orden.numeroorden : undefined;
  }

  private _filter2(orden: number): OrdenFarmacia[] {
    const filterValue = orden.toString().toLowerCase();
    return this.ordenes.filter(orden => orden.numeroorden.toString().toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    this.productoseleccionado=event.option.value;
    this.idproductoseleccionado=this.productoseleccionado.idproducto;
    this.productostock=this.productoseleccionado;
    this.stock=this.productostock.cantidadproducto;
    console.log(this.productoseleccionado);
  }

  desc(event: MatSelectChange) {
    this.descuento=event.source.value;
    console.log(this.descuento)

  }

  onSelectionChanged2(event: MatAutocompleteSelectedEvent) {
    this.comprobanteseleccionado = event.option.value;
    let numero = this.comprobanteseleccionado.numeroorden;
    console.log(this.comprobanteseleccionado );
    this.ordenfarmaciaService.reporteOrdenFPaciente(numero).subscribe(data =>{
      this.cpunit = data;
    })
  }

  // prueba(event: MatSelectChange) {
  //   const selectedData = {
  //     text: (event.source.selected as MatOption).viewValue,
  //     value: event.source.value
  //   }
  //   this.productostock = selectedData.value;
  //   this.stock=this.productostock.cantidadproducto;
  // }

  companterior(){
    this.ordenfarmaciaService.listarOrdenFarmaciaId().subscribe(data=>{
      this.ultimaorden=data;
      console.log(this.ultimaorden);
      if(this.ultimaorden!=null){
      var numero= this.ultimaorden.numeroorden + 1;
      }else{
        numero=1;
      }

      let numeronuevo=numero.toString();
      let cero = "0";
      for(var _i=0;_i < 7-numeronuevo.length; _i++){
        cero=cero+"0";
      }
      this.numeroorden=cero+numeronuevo;
      console.log(parseInt(this.numeroorden));
    });
  }

  agregar(){

    if (this.form1.valid === true && this.producto.valid===true) {

      if (this.form1.value['cantidadfarmacia']<=this.stock) {
      
        let det = new DetalleOF();
      det.cantidad=this.form1.value['cantidadfarmacia'];
      det.producto=this.productoseleccionado;
      det.importe=det.cantidad*det.producto.pventaproducto;

      this.detalles.push(det);
      this.dataSource = new MatTableDataSource(this.detalles);
      this.producto.setValue("");
      this.productoseleccionado.presentacionproducto.cantidadpresentacionproducto
        }else{
          this.ordenfarmaciaService.mensaje.next('La cantidad ingresada del producto debe ser menor o igual al stock');
        }
      }
      else{
        this.ordenfarmaciaService.mensaje.next('Usted no ha seleccionado un producto');
      }

  }

  getTotalCost() {
    this.total = this.detalles.map(t => t.importe).reduce((acc, value) => acc + value, 0);
    return this.detalles.map(t => t.importe).reduce((acc, value) => acc + value, 0);
  }

  operar(){
    this.ordenFarmacia.consumidorordenfarmacia=this.form.value['consumidorordenfarmacia'];
    this.ordenFarmacia.numeroorden=parseInt(this.numeroorden);
    this.ordenFarmacia.fechaordenfarmacia=this.fechaSeleccionada;
    this.ordenFarmacia.rucordenfarmacia=this.form.value['rucordenfarmacia'];
    this.ordenFarmacia.estado=1;
    this.ordenFarmacia.descuento=this.descuento;
    this.ordenFarmacia.detalleordenF=this.detalles;
    this.ordenFarmacia.tipo=1;
    this.ordenFarmacia.serie=1;

    let totalfinal=this.total*this.descuento*(1/100);
    this.ordenFarmacia.total=this.total-totalfinal;
    if (this.form.valid ===true) {
            for (let i = 0; i < this.detalles.length; i++) {
              this.productoService.listarproductoporNombre(this.detalles[i].producto.idproducto).subscribe(data=>{
                data.cantidadproducto = data.cantidadproducto-this.detalles[i].cantidad;
                this.productoService.modificarproducto(data).subscribe(dataa=>{});         
              });       
            }
        this.ordenfarmaciaService.registrarOrdenFarmacia(this.ordenFarmacia).subscribe(data =>{
          this.ordenfarmaciaService.mensaje.next('Se registró correctamente');
        });
        console.log(this.ordenFarmacia);
    }else{
      this.ordenfarmaciaService.mensaje.next('Falta algún dato requerido');
    }
  }

  cancelar(){
    window.location.reload();
  }

  eliminar(transaction){
  var indice=this.detalles.indexOf(transaction);
  this.detalles.splice(indice, 1);
  this.dataSource = new MatTableDataSource(this.detalles);
  }

  imprimir(){
    let a=parseInt(this.numeroorden);
    console.log(a);
    this.ordenfarmaciaService.reporteOrdenFPaciente(a).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    })
  }


  pdfDetallado(){
    if (this.fechaDetallada!=null) {
      let s1 = moment(this.fechaDetallada).format("DD-MM-YYYY");
      this.ordenfarmaciaService.reporteDetalladoFarmacia(s1).subscribe(data=>{
        var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
        var fileURL = window.URL.createObjectURL(data2);
        window.open(fileURL,"_blank");
        // this.exportToExcel(this.vent,'Detallado');
      })
  }else{
    this.ordenfarmaciaService.mensaje.next('Seleccione la fecha a consultar');
  }
  }


  eliminarCP(){
    if (this.comprobanteseleccionado!=null && this.comprobanteseleccionado.idordenfarmacia>0) {
      this.comprobanteseleccionado.estado=0;
      this.ordenfarmaciaService.modificarOrdenFarmacia(this.comprobanteseleccionado).subscribe(data=>{
        this.ordenfarmaciaService.mensaje.next('El recibo se eliminó correctamente');
        this.comprobanteseleccionado=null;
      });
      this.comprobante.setValue('');
    }else{
      this.ordenfarmaciaService.mensaje.next('Ingrese el número del recibo a eliminar')
    }
  }


  cancelarof(){
    this.form.reset();
    this.ordenfarmaciaService.mensaje.next('Se canceló el procedimiento');
    window.location.reload();
  }

  pdfDetalladoXLSX(){
    if (this.fechaDetallada!=null) {
      let s1 = moment(this.fechaDetallada).format("DD-MM-YYYY");
      this.ordenfarmaciaService.reporteDetalladoFarmaciaXLSX(s1).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
      FileSaver.saveAs(data2,"VDetallado-"+s1+".xlsx");
      })
  }else{
    this.ordenfarmaciaService.mensaje.next('Seleccione la fecha a consultar');
  }
  }

  buscarrec(){
    if (this.numerorecib!=null) {
      this.ordenfarmaciaService.buscarOrdenFarmaciaVigente(this.numerorecib).subscribe(data=>{
        this.ordenseleccionada=data;
        this.ordenfarmaciaService.reporteOrdenFPaciente(this.numerorecib).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
      })
    } else {
      this.ordenfarmaciaService.mensaje.next('Ingrese el número de recibo a consultar')
    }

  }

  nota(){
    if (this.ordenseleccionada!=null && this.ordenseleccionada.idordenfarmacia>0) {
      let pro = this.ordenseleccionada;
      let dialogRef = this.dialog.open(NotacreditoOrdenComponent, {
        width: '95%',   
        disableClose: false,
        data: pro
      });
    } else {
      this.ordenfarmaciaService.mensaje.next("Busque el comprobante antes de generar la nota de crédito")
    }
  }


  DetalladoNCXLSX(){
    if (this.fechainicio!=null && this.fechafin!=null 
      && this.fechainicio<=this.fechafin) {
      let s1 = moment(this.fechainicio).format("DD-MM-YYYY");
      let s2 = moment(this.fechafin).format("DD-MM-YYYY");
      this.ordenfarmaciaService.reporteNCFarmaciaXLSX(s1,s2).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
      FileSaver.saveAs(data2,"VDetallado-"+s1+".xlsx");
      })
  }else{
    this.ordenfarmaciaService.mensaje.next('Seleccione la fecha a consultar');
  }
  }

  pdfDetalladoNC(){
    if (this.fechainicio!=null && this.fechafin!=null 
      && this.fechainicio<=this.fechafin) {
        let s1 = moment(this.fechainicio).format("DD-MM-YYYY");
        let s2 = moment(this.fechafin).format("DD-MM-YYYY");
      this.ordenfarmaciaService.reporteNCFarmaciaPDF(s1,s2).subscribe(data=>{
        var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
        var fileURL = window.URL.createObjectURL(data2);
        window.open(fileURL,"_blank");
        // this.exportToExcel(this.vent,'Detallado');
      })
  }else{
    this.ordenfarmaciaService.mensaje.next('Seleccione la fecha a consultar');
  }
  }

}
