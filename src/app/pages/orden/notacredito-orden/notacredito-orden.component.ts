import { DetalleOF } from './../../../_model/detalleof';
import { startWith, map } from 'rxjs/operators';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Producto } from './../../../_model/producto';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatTableDataSource } from '@angular/material';
import { OrdenFarmacia } from './../../../_model/orden';
import { OrdenfarmaciaService } from './../../../_service/ordenfarmacia.service';
import { ProductoService } from './../../../_service/producto.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-notacredito-orden',
  templateUrl: './notacredito-orden.component.html',
  styleUrls: ['./notacredito-orden.component.css']
})
export class NotacreditoOrdenComponent implements OnInit {

  displayedColumns: string[] = ['cantidaddetalle', 'denominacionproducto','presentacion','pventaproducto','importedetalle', 'acciones'];
  productos: Producto[] = [];
  productoseleccionado: Producto;

  producto=new FormControl();
  filteredOptions: Observable<Producto[]>;

  ultimaorden:OrdenFarmacia;
  numeroorden:string;
  notacredito=new OrdenFarmacia();
  descuentos=[0,50,100];

  productostock:Producto;
  idproductoseleccionado: number;
  stock:number;

  form1:FormGroup;

  total:number;
  detalle:DetalleOF;
  dataSource: MatTableDataSource<DetalleOF>;
  detalles:DetalleOF[]=[];
  
  descuento:number;


  constructor(
    private ordenfarmaciaService:OrdenfarmaciaService,
    private productoService:ProductoService,public dialogRef: MatDialogRef<NotacreditoOrdenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OrdenFarmacia) {
    this.form1=new FormGroup({
      'cantidadfarmacia':new FormControl(1),
      'stock': new FormControl({value:0 , disabled:true}, Validators.required),
    });
   }

  ngOnInit() {
    this.notacredito=this.data;
    this.descuento=this.notacredito.descuento;
    this.detalles=this.data.detalleordenF;
    this.dataSource = new MatTableDataSource(this.detalles);
    this.notacredito.fechaordenref=this.data.fechaordenfarmacia;
    this.notacredito.serieref=this.data.serie;
    this.notacredito.tiporef=this.data.tipo;
    this.notacredito.numeroref=this.data.numeroorden;
    this.notacredito.totalref=this.data.total;

    for (let i = 0; i < this.detalles.length; i++) {
      this.detalles[i].iddetalleordenf = null;
    }
    console.log(this.detalles)
    console.log(this.notacredito)

    this.companterior();
    this.listarproducto();

    this.filteredOptions = this.producto.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombreproducto),
      map(nombreproducto => nombreproducto ? this._filter(nombreproducto) : this.productos.slice())
    );
  }

  listarproducto(){
    this.productoService.listarproductos().subscribe(data => {
      this.productos=data;
    })
  }

  companterior(){
    this.ordenfarmaciaService.listarOrdenFarmaciaId().subscribe(data=>{
      this.ultimaorden=data;
      if(this.ultimaorden!=null){
      var numero= this.ultimaorden.numeroorden + 1;
      }else{
        numero=1;
      }

      let numeronuevo=numero.toString();
      let cero = "0";
      for(var _i=0;_i < 7-numeronuevo.length; _i++){
        cero=cero+"0";
      }
      this.numeroorden=cero+numeronuevo;
    });
  }

  displayFn(producto?: Producto): string | undefined {
    return producto ? producto.nombreproducto : undefined;
  }

  private _filter(nombreproducto: string): Producto[] {
    const filterValue = nombreproducto.toLowerCase();
    return this.productos.filter(producto => producto.nombreproducto.toLowerCase().indexOf(filterValue) === 0);
  }


  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    this.productoseleccionado=event.option.value;
    this.idproductoseleccionado=this.productoseleccionado.idproducto;
    this.productostock=this.productoseleccionado;
    this.stock=this.productostock.cantidadproducto;
    console.log(this.productoseleccionado);
  }


  agregar(){

    if (this.form1.valid === true && this.producto.valid===true) {

      if (this.form1.value['cantidadfarmacia']<=this.stock) {
      
        let det = new DetalleOF();
      det.cantidad=this.form1.value['cantidadfarmacia'];
      det.producto=this.productoseleccionado;
      det.importe=det.cantidad*det.producto.pventaproducto;

      this.detalles.push(det);
      this.dataSource = new MatTableDataSource(this.detalles);
      this.producto.setValue("");
      this.productoseleccionado.presentacionproducto.cantidadpresentacionproducto
        }else{
          this.ordenfarmaciaService.mensaje.next('La cantidad ingresada del producto debe ser menor o igual al stock');
        }
      }
      else{
        this.ordenfarmaciaService.mensaje.next('Usted no ha seleccionado un producto');
      }
  }

  getTotalCost() {
    this.total = this.detalles.map(t => t.cantidad*t.producto.pventaproducto).reduce((acc, value) => acc + value, 0);
    return this.detalles.map(t => t.cantidad*t.producto.pventaproducto).reduce((acc, value) => acc + value, 0);
  }

  eliminar(transaction){
    var indice=this.detalles.indexOf(transaction);
    this.detalles.splice(indice, 1);
    this.dataSource = new MatTableDataSource(this.detalles);
    }


    operar(){
      this.notacredito.idordenfarmacia=null;
      this.notacredito.numeroorden=parseInt(this.numeroorden);
      this.notacredito.estado=1;
      this.notacredito.serie=1;
      this.notacredito.tipo=2;
      this.notacredito.descuento=0;
      this.notacredito.detalleordenF=this.detalles;
  
      let totalfinal=this.total*this.notacredito.descuento*(1/100);
      this.notacredito.total=(this.total-totalfinal)*(-1);
      if (this.notacredito != null) {

              for (let i = 0; i < this.detalles.length; i++) {
                this.productoService.listarproductoporNombre(this.detalles[i].producto.idproducto).subscribe(data=>{
                  data.cantidadproducto = data.cantidadproducto+this.detalles[i].cantidad;
                  this.productoService.modificarproducto(data).subscribe(dataa=>{});         
                });       
              }
          this.ordenfarmaciaService.registrarOrdenFarmacia(this.notacredito).subscribe(data =>{
            this.ordenfarmaciaService.mensaje.next('Se registró correctamente');
          });
          console.log(this.notacredito);
      }else{
        this.ordenfarmaciaService.mensaje.next('Falta algún dato requerido');
      }
    }



    cancelar(){
      window.location.reload();
    }

    
  imprimir(){
    let a=parseInt(this.numeroorden);
    console.log(a);
    this.ordenfarmaciaService.reporteOrdenFPaciente(a).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    })
  }

}
