import { ExamenesgService } from './../../_service/examenesg.service';
import { Examenesg } from './../../_model/examenesg';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-examenesgenerales',
  templateUrl: './examenesgenerales.component.html',
  styleUrls: ['./examenesgenerales.component.css']
})
export class ExamenesgeneralesComponent implements OnInit {

  lista: Examenesg[]=[];
  displayedColumns=['idexamenesg','denominacion','descripcion','acciones'];
  dataSource: MatTableDataSource<Examenesg>;
  maxFecha: Date = new Date();
  fechaSeleccionada1: Date = null;
  fechaSeleccionada2: Date = null;
  fechaSeleccionada3: Date = null;
  fechaSeleccionada4: Date = null;

  rep:any='';

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private examenesgService:ExamenesgService,
    private matSnackBar:MatSnackBar) { }

  ngOnInit() {
    this.examenesgService.examenesgCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.examenesgService.mensaje.subscribe(data =>{
        this.matSnackBar.open(data, 'Aviso',{duration:3000});
      });
    });


    this.examenesgService.listarExamenesg().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }

  pdfReporteExamenesG(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.examenesgService.reporteBioquimica(s1,s2).subscribe(data=>{
          this.rep=data;
        })
    }else{
      this.examenesgService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfReporteLab(){
    if (this.fechaSeleccionada3!=null && this.fechaSeleccionada4!=null 
      && this.fechaSeleccionada3<this.fechaSeleccionada4) {
        let s3 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        let s4 = moment(this.fechaSeleccionada4).format("DD-MM-YYYY");
        this.examenesgService.reporteLab(s3,s4).subscribe(data=>{
          this.rep=data;
        })
    }else{
      this.examenesgService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }
}
