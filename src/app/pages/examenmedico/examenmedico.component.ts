import { DialogexamenmedicoComponent } from './dialogexamenmedico/dialogexamenmedico.component';
import { ExamenmedicoService } from './../../_service/examenmedico.service';
import { ExamenMedico } from './../../_model/examenmedico';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-examenmedico',
  templateUrl: './examenmedico.component.html',
  styleUrls: ['./examenmedico.component.css']
})
export class ExamenmedicoComponent implements OnInit {

  examenesmedicos: ExamenMedico[] = [];
  displayedColumns = ['idexamenmedico', 'denominacionexamenmedico','categoriaexamenmedico','orden','acciones'];
  dataSource: MatTableDataSource<ExamenMedico>;
  mensaje: string;
  row:ExamenMedico;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  fechaSeleccionada1: Date = null;
  fechaSeleccionada2: Date = null;
  rep:any='';

  constructor(private examenmedicoService:ExamenmedicoService,
              public dialog:MatDialog, 
              public snackBar:MatSnackBar) { }


ngOnInit() {
    this.examenmedicoService.examenmedicoCambio.subscribe(data => {
      this.examenesmedicos = data;
      this.dataSource = new MatTableDataSource(this.examenesmedicos);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.examenmedicoService.mensaje.subscribe(data => {
      console.log(data);
      this.snackBar.open(data, null, { duration: 2000 });
    });

    this.examenmedicoService.listarEMedico().subscribe(data => {
      this.examenesmedicos = data;

      this.dataSource = new MatTableDataSource(this.examenesmedicos);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  openDialog(examenmedico: ExamenMedico): void {

    let pro = examenmedico != null ? examenmedico : new ExamenMedico();
    let dialogRef = this.dialog.open(DialogexamenmedicoComponent, {
      width: '500px',   
      disableClose: true,   
      
      data: pro      
    });
  }
  
  pdfCaja(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.examenmedicoService.reporteLaboratorio(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.examenmedicoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfLabxlsx(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.examenmedicoService.reporteLaboratorioXLSX(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"LabGeneral"+s1+"-"+s2+".xlsx");
        })
    }else{
      this.examenmedicoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

}