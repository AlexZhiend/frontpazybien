import { startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ExamenmedicoService } from './../../../_service/examenmedico.service';
import { CategoriaexamenmedicoService } from './../../../_service/categoriaexamenmedico.service';
import { Categoriaexamenmedico } from './../../../_model/categoriaexamenmedico';
import { Component, OnInit, Inject } from '@angular/core';
import { ExamenMedico } from 'src/app/_model/examenmedico';
import { MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';

@Component({
  selector: 'app-dialogexamenmedico',
  templateUrl: './dialogexamenmedico.component.html',
  styleUrls: ['./dialogexamenmedico.component.css']
})
export class DialogexamenmedicoComponent implements OnInit {

  examenmedico: ExamenMedico;
  categoriaexamenmedico: Categoriaexamenmedico[] = []; 
  idCategoriaSeleccionado: number;

  categoria=new FormControl();
  filteredOptions1: Observable<Categoriaexamenmedico[]>;
  categoriaexamenseleccionado= new Categoriaexamenmedico();


  constructor(public dialogRef: MatDialogRef<DialogexamenmedicoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExamenMedico,
    private categoriaexamenmedicoService:CategoriaexamenmedicoService,
    private examenmedicoService:ExamenmedicoService) { }

  ngOnInit() {

      this.listarCategorias();

      this.filteredOptions1 = this.categoria.valueChanges.pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombrecategoriaexamenmedico),
        map(nombrecategoriaexamenmedico => nombrecategoriaexamenmedico ? this._filter1(nombrecategoriaexamenmedico) : this.categoriaexamenmedico.slice())
      );

      this.examenmedico = new ExamenMedico();
      this.examenmedico.idexamenmedico=this.data.idexamenmedico;
      this.examenmedico.denominacionexamenmedico = this.data.denominacionexamenmedico;
      this.examenmedico.unidadmedida = this.data.unidadmedida;
      this.examenmedico.valorreferencia = this.data.valorreferencia;
      this.examenmedico.extra = this.data.extra;

      if(this.data.idexamenmedico!= null){
        this.categoria.setValue(this.data.categoriaexamenmedico);
        this.categoriaexamenseleccionado=this.data.categoriaexamenmedico;
      }
    }

    listarCategorias() {
      this.categoriaexamenmedicoService.listarCategoriaExamen().subscribe(data => {
        this.categoriaexamenmedico = data;
      });
    }

    displayFn1(categoriaexamen?: Categoriaexamenmedico): string | undefined {
      return categoriaexamen ? categoriaexamen.nombrecategoriaexamenmedico : undefined;
    }

    private _filter1(nombrecategoriaexamenmedico: string): Categoriaexamenmedico[] {
      const filterValue = nombrecategoriaexamenmedico.toLowerCase();
      return this.categoriaexamenmedico.filter(categoria => categoria.nombrecategoriaexamenmedico.toLowerCase().indexOf(filterValue) === 0);
    }

    onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
      this.categoriaexamenseleccionado=event.option.value;
      console.log(this.categoriaexamenseleccionado);
    }
  
 
    operar(){
      
      if(this.examenmedico != null && this.examenmedico.idexamenmedico > 0){

        let categoria = new Categoriaexamenmedico();
        categoria.idcategoriaexamenmedico= this.categoriaexamenseleccionado.idcategoriaexamenmedico;

        this.examenmedico.categoriaexamenmedico=categoria;
        console.log(this.examenmedico);
        this.examenmedicoService.modificarEMedico(this.examenmedico).subscribe(data => {
          
            this.examenmedicoService.listarEMedico().subscribe(examenmedicos => {
              this.examenmedicoService.examenmedicoCambio.next(examenmedicos);
              this.examenmedicoService.mensaje.next("Se modificó correctamente");
            });
            this.dialogRef.close();
        });
      }else{

        let categoria = new Categoriaexamenmedico();
        categoria.idcategoriaexamenmedico= this.categoriaexamenseleccionado.idcategoriaexamenmedico;

        this.examenmedico.categoriaexamenmedico=categoria;


        this.examenmedicoService.registrarEMedico(this.examenmedico).subscribe(data => {
          
            this.examenmedicoService.listarEMedico().subscribe(examenmedicos => {
              this.examenmedicoService.examenmedicoCambio.next(examenmedicos);
              this.examenmedicoService.mensaje.next("Se registró correctamente");
            });
          
        });
      }
      this.dialogRef.close();
    }
  
    cancelar(){
      this.dialogRef.close();
      this.examenmedicoService.mensaje.next('se canceló el procedimiento');
    }
    

}
