import { ExamenMedico } from './../../../_model/examenmedico';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { Resultados } from 'src/app/_model/resultados';

@Component({
  selector: 'app-dialogexameneg',
  templateUrl: './dialogexameneg.component.html',
  styleUrls: ['./dialogexameneg.component.css']
})
export class DialogexamenegComponent implements OnInit {

  idpaciente:number;
  examenmedico:ExamenMedico;
  resultados:Resultados;

  constructor(public dialogRef: MatDialogRef<DialogexamenegComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExamenMedico) { }

  ngOnInit() {
    this.data;
    this.idpaciente=parseInt(localStorage.getItem("idpacientelab"));
    console.log(this.data);
    console.log(this.idpaciente);
  }

}
