import { PacienteService } from './../../_service/paciente.service';
import { ResultadosService } from './../../_service/resultados.service';
import { DialogexamenegComponent } from './dialogexameneg/dialogexameneg.component';
import { Paciente } from './../../_model/paciente';
import { ExamenMedico } from './../../_model/examenmedico';
import { ExamenmedicoService } from './../../_service/examenmedico.service';
import { Categoriaexamenmedico } from './../../_model/categoriaexamenmedico';
import { CategoriaexamenmedicoService } from './../../_service/categoriaexamenmedico.service';
import { Observable } from 'rxjs';

import { MatTableDataSource, MatSnackBar, MatSelectChange, MatOption, MatAutocompleteSelectedEvent, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { DetalleegService } from './../../_service/detalleeg.service';
import { ComprobantePago } from 'src/app/_model/comprobantepago';
import { ComprobantepagoService } from './../../_service/comprobantepago.service';
import { ExamenesgService } from './../../_service/examenesg.service';
import { Examenesg } from './../../_model/examenesg';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DetalleExamen } from 'src/app/_model/detalleeg';
import { FormGroup, FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import * as moment from 'moment';
import { Resultados } from 'src/app/_model/resultados';

@Component({
  selector: 'app-detalleeg',
  templateUrl: './detalleeg.component.html',
  styleUrls: ['./detalleeg.component.css']
})
export class DetalleegComponent implements OnInit {
 
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  //autocomplete paciente
  form2: FormGroup;
  comprobante = new FormControl();
  filteredOptions: Observable<ComprobantePago[]>;
  comprobantepagos: ComprobantePago[] = []; 
  comprobanteseleccionado= new ComprobantePago();
  paciente:string;


//datasource1
  categoriaexamenmedicos: Categoriaexamenmedico[] = [];
  displayedColumns = ['nombre','acciones'];
  dataSource: MatTableDataSource<Categoriaexamenmedico>;
  row:Categoriaexamenmedico;

  //datasource2
  examenesmedicos: ExamenMedico[] = [];
  displayedColumns1 = ['denominacionexamenmedico','unidadmedida','extra','acciones'];
  dataSource1: MatTableDataSource<ExamenMedico>;
  idpaciente:number;
  pacienteSeleccioando= new Paciente();

  //datasource2
  detalleexamenes: DetalleExamen[]=[];
  displayedColumns2 = ['denominacionexamenmedico','resultado','unidadmedida','valorreferencia','extra','categoria','acciones'];
  dataSource2: MatTableDataSource<DetalleExamen>;

  //resultados
  resultado= new Resultados();
  form1:FormGroup;
  examenresultado= new ExamenMedico();
  detalleseleccionado=new DetalleExamen();
  observacioness:string;
  
  ex:any='';

  //autocomplete
  // pacient=new FormControl();
  pacientes: Paciente[]=[];
  // filteredOptions2: Observable<Paciente[]>;
  pacienteresultado= new Paciente();
  //datasourve3
  displayedColumns3 = ['idresultados','fecha','observaciones','acciones'];
  dataSource3: MatTableDataSource<Resultados>;
  resultadospaciente: Resultados[]=[];

  res:any ='';

  num:number;
  recibo= new ComprobantePago();

  idresult:Resultados;
  nombres:String='';


  //Lista de paciente
  dataSourcePac: MatTableDataSource<Paciente>;
  displayedColumnsPac = ['hcl', 'dnipaciente', 'nombresyapellidos','tipopaciente','acciones'];
  rows:Paciente;


  pacient:string;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private comprobantepagoService:ComprobantepagoService,
              private categoriaexamenmedicoService:CategoriaexamenmedicoService,
              private examenmedicoService:ExamenmedicoService,
              private resultadosService:ResultadosService,
              private detalleegService:DetalleegService,
              private pacienteService:PacienteService,
              public dialog:MatDialog,
              public snackBar:MatSnackBar) {
    this.form2=new FormGroup({
      'comprobante': new FormControl(0),
      'fechaSeleccionada': new FormControl(new Date),
    });
    this.form1=new FormGroup({
      'resultado': new FormControl('')
    });
  }

  ngOnInit() {

    // this.listarPaciente();

    this.resultadosService.mensaje.subscribe(data => {
      console.log(data);
      this.snackBar.open(data, null, { duration: 3000 });
    });
    
    // this.filteredOptions2 = this.pacient.valueChanges.pipe(
    //   startWith(''),
    //   map(value => typeof value === 'string' ? value : value.nombresyapellidos),
    //   map(nombresyapellidos => nombresyapellidos ? this._filter1(nombresyapellidos) : this.pacientes.slice())
    // );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSourcePac.filter = filterValue;
  }

  displayFn(comprobantepago?: ComprobantePago): number | undefined {
    return comprobantepago ? comprobantepago.numerorecibocomprobante : undefined;
  }

  //  listarPaciente() {
  //   this.pacienteService.listarpaciente().subscribe(data => {
  //     this.pacientes = data;
  //   });
  // }

  // displayFn1(paciente?: Paciente): string | undefined {
  //   return paciente ? paciente.nombresyapellidos : undefined;
  // }
  
  // private _filter1(nombresyapellidos: string): Paciente[] {
  //   const filterValue = nombresyapellidos.toLowerCase();
  //   return this.pacientes.filter(paciente => paciente.nombresyapellidos.toLowerCase().indexOf(filterValue) === 0);
  // }

  onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
    this.pacienteresultado=event.option.value;
    this.resultadosService.listarResultadosId(this.pacienteresultado.idpaciente).subscribe(data=>{
      console.log(data);
      this.resultadospaciente=data;
      this.dataSource3= new MatTableDataSource(this.resultadospaciente);
      this.dataSource3.sort=this.sort;
    })
    console.log(this.resultadospaciente)
  }

  // onSelectionChanged(event: MatAutocompleteSelectedEvent) {
  //   this.comprobanteseleccionado = event.option.value;
  //   if (this.comprobanteseleccionado.paciente==null) {
  //     this.paciente="Paciente externo";
  //   }else{
  //     this.paciente=this.comprobanteseleccionado.paciente.nombresyapellidos;
  //   }

  //   this.categoriaexamenmedicoService.listarCategoriaporComprobanteId(this.comprobanteseleccionado.idcomprobantepago).subscribe(data=>{
  //     this.categoriaexamenmedicos= data;
  //     this.dataSource=new MatTableDataSource(this.categoriaexamenmedicos);
  //   })
  //   this.idpaciente=this.comprobanteseleccionado.paciente.idpaciente;
  //   localStorage.setItem("idpacientelab",this.idpaciente.toString());
  //   this.pacienteSeleccioando=this.comprobanteseleccionado.paciente;
  // }

  llamarexamen(categoria:Categoriaexamenmedico){
    this.examenmedicoService.listarExamenPorCategoriaid(categoria.idcategoriaexamenmedico).subscribe(data=>{
      this.examenesmedicos=data;
      this.examenesmedicos.sort((a, b) => a.extra - b.extra);
      console.log(this.examenesmedicos);
      this.dataSource1= new MatTableDataSource(this.examenesmedicos);
    })
  }

  // registrar(examen:ExamenMedico): void{
  //   let pro = examen != null ? examen : new ExamenMedico();
  //   let id = this.idpaciente != null ? this.idpaciente:Number;
  //   let dialogRef = this.dialog.open(DialogexamenegComponent, {
  //     width: '500px',   
  //     disableClose: false,   
      
  //     data: pro
  //   });
  // }

  
  resultadoex(examen:ExamenMedico){
    this.examenresultado=examen;
  }

  agregar(){
    if (this.form1.valid===true&&this.examenresultado!=null
      &&this.examenresultado.denominacionexamenmedico!=null
      &&this.examenresultado.unidadmedida!=null) {
      let det = new DetalleExamen();
      det.examenmedico= this.examenresultado;
      det.resultado=this.form1.value['resultado'];

      this.detalleexamenes.push(det);
      this.dataSource2 = new MatTableDataSource(this.detalleexamenes);
      this.form1.reset();

    let indice2 = this.examenesmedicos.indexOf(this.examenresultado);
    this.examenesmedicos.splice(indice2,1);
    this.dataSource1 = new MatTableDataSource(this.examenesmedicos);

    this.examenresultado=new ExamenMedico();
    this.examenresultado.denominacionexamenmedico=null;
    this.examenresultado.unidadmedida=null;

    }
    else{
      this.resultadosService.mensaje.next('Seleccione el examen o revise si falta ingresar datos en el resultado del examen');
    }
  }

  eliminar(detalle:DetalleExamen){
    var indice = this.detalleexamenes.indexOf(detalle);
    this.detalleexamenes.splice(indice, 1);
    this.dataSource2 = new MatTableDataSource(this.detalleexamenes);

    this.detalleseleccionado = detalle;
    this.examenresultado = detalle.examenmedico;
    this.examenesmedicos.push(detalle.examenmedico);
    this.examenesmedicos.sort((a, b) => a.extra - b.extra);
    this.dataSource1 = new MatTableDataSource(this.examenesmedicos);
  }

  registrar(){
    if (this.form2.valid===true&&this.paciente!=null&&this.detalleexamenes.length!=0) {
      let pac = new Paciente();
      pac.idpaciente=this.pacienteSeleccioando.idpaciente;
      this.resultado.paciente=pac;
      this.resultado.observaciones=this.observacioness;
      this.resultado.detalleexamen=this.detalleexamenes;
      this.resultado.fecha=this.form2.value['fechaSeleccionada'];
      let s1 = moment(this.form2.value['fechaSeleccionada']).format("DD-MM-YYYY");
      console.log(this.resultado);
      this.resultadosService.registrarResultados(this.resultado).subscribe(data=>{
        this.resultadosService.mensaje.next('Se registraron los exámenes correctamente');
        this.detalleegService.reporteExamenesgPaciente(this.pacienteSeleccioando.idpaciente,s1).subscribe(data =>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
      });
      // this.detalleegService.reporteExamenesgPaciente(this.pacienteSeleccioando.idpaciente,"28-11-2019").subscribe(data =>{
      //   this.ex=data;
      // })
    }
    else{
      this.resultadosService.mensaje.next('Falta ingresar un dato, porfavor revise los formularios')
    }
  }

  terminar(){
    window.location.reload();
  }

  pdf2(resultado:Resultados){
    
    let s1 = moment(resultado.fecha).format("DD-MM-YYYY");
    console.log(s1);
    this.detalleegService.reporteExamenesgPaciente(resultado.paciente.idpaciente,s1).subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    })
  }



  consultar(){
    if (this.num!=0) {
      this.comprobantepagoService.BuscarComprobanteporNum(this.num).subscribe(data=>{
        if (data==null) {
          this.comprobantepagoService.mensaje.next('No se encontraron coincidencias');
        }
        this.comprobanteseleccionado=data;

        if (this.comprobanteseleccionado.paciente==null) {
          this.paciente="Paciente externo";
        }else{
          this.paciente=this.comprobanteseleccionado.paciente.nombresyapellidos;
        }

        this.categoriaexamenmedicoService.listarCategoriaporComprobanteId(this.comprobanteseleccionado.idcomprobantepago).subscribe(data=>{
          this.categoriaexamenmedicos= data;
          this.dataSource=new MatTableDataSource(this.categoriaexamenmedicos);
        })
        this.idpaciente=this.comprobanteseleccionado.paciente.idpaciente;
        localStorage.setItem("idpacientelab",this.idpaciente.toString());
        this.pacienteSeleccioando=this.comprobanteseleccionado.paciente;
      })
    }else{
      this.comprobantepagoService.mensaje.next('Ingrese un indicio del nombre a buscar')
    }
  }

  seleccionar(row:Resultados){
    this.idresult = row;
    this.nombres=this.idresult.paciente.nombresyapellidos;
    console.log(this.idresult)
  }

  eliminarhist(){
    if (this.idresult!=null && this.idresult.idresultados>0) {
      console.log(this.idresult);
      this.resultadosService.eliminarRes(this.idresult.idresultados).subscribe(data=>{
        this.resultadosService.mensaje.next('La historia se eliminó correctamente');
        this.resultadosService.listarResultadosId(this.pacienteresultado.idpaciente).subscribe(data =>{
          this.resultadospaciente=data;
          this.dataSource3= new MatTableDataSource(this.resultadospaciente);
          this.dataSource3.sort=this.sort;
        });
      });

    }else{
      this.resultadosService.mensaje.next('Error al eliminar la historia')
    }

  }

  consultarpac(){
    if (this.pacient!='') {
      this.pacienteService.buscarPorNombre(this.pacient).subscribe(data=>{
        if (data.length==0) {
          this.pacienteService.mensaje.next('No se encontraron coincidencias');
        }
        this.pacientes=data;
        this.dataSourcePac = new MatTableDataSource(this.pacientes);
        this.dataSourcePac.paginator = this.paginator;
        this.dataSourcePac.sort = this.sort;
      })
    }else{
      this.pacienteService.mensaje.next('Ingrese un indicio del nombre a buscar')
    }
  }

  cargar(rows :Paciente){
    this.pacienteresultado=rows;
    this.resultadosService.listarResultadosId(this.pacienteresultado.idpaciente).subscribe(data=>{
      console.log(data);
      if (data.length > 0 ) {
        this.resultadospaciente=data;
        this.dataSource3= new MatTableDataSource(this.resultadospaciente);
        this.dataSource3.sort=this.sort;
        
      } else {
        this.resultadosService.mensaje.next('El paciente no tiene resultados')
      }

    })
  }

}
