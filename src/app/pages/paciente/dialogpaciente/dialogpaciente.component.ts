
import { WebsocketService } from './../../../_service/websocket.service';
import { TipopacienteService } from './../../../_service/tipopaciente.service';
import { PacienteService } from './../../../_service/paciente.service';
import { Tipopaciente } from './../../../_model/tipopaciente';
import { Component, OnInit, Inject } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';

@Component({
  selector: 'app-dialogpaciente',
  templateUrl: './dialogpaciente.component.html',
  styleUrls: ['./dialogpaciente.component.css']
})
export class DialogpacienteComponent implements OnInit {
  paciente: Paciente;
  tipopacientes: Tipopaciente[] = []; 
  idTipopacienteSeleccionado: number;


  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  hclinterno= new Paciente();
  hclexterno=new Paciente();

  civil:any = ['Soltero(a)','Casado(a)','Divorciado(a)','Viudo(a)','Conviviente'];

  niveleducativo:any=['Infante','Primaria','Secundaria','Superior técnico','Superior universitario','No estudios'];
  idUsuarioSocket: number;
  estadoexterno: number;
  verificacion: Paciente=null;
  // alertResponse;
  
  // urlSocket = 'ws://'+HOST_SOCKET+'/client?user_id=';
  // public notificacion:Subject<any>;


  constructor(public dialogRef: MatDialogRef<DialogpacienteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Paciente,
    private pacienteService:PacienteService,
    private tipopacienteService:TipopacienteService,private wsService:WebsocketService) { 
      
    }

ngOnInit() {
    document.addEventListener("hclEvent", event=> {        
      var a=parseInt(window.localStorage.getItem('hcl'));
      console.log('hcl desde localstorage: '+a);
      this.hclinterno.hcl=a;      
    });
    if (sessionStorage.getItem("nombre")=="triajet") {
      this.idUsuarioSocket = 0;
      console.log(this.idUsuarioSocket)
    }else{
      this.idUsuarioSocket = 2;
      console.log(this.idUsuarioSocket)
    }
    this.listarInterno();
    this.listarExterno();
      this.listarTipopaciente();
      this.paciente = new Paciente();
      this.paciente.idpaciente=this.data.idpaciente;
      this.paciente.hcl=this.data.hcl;
      this.paciente.nombresyapellidos = this.data.nombresyapellidos;
      this.paciente.dnipaciente = this.data.dnipaciente;
      this.paciente.fechanacimientopaciente = this.data.fechanacimientopaciente;
      this.paciente.departamentopaciente = this.data.departamentopaciente;
      this.paciente.provinciapaciente = this.data.provinciapaciente;
      this.paciente.distritopaciente = this.data.distritopaciente;
      this.paciente.direccionpaciente = this.data.direccionpaciente;
      this.paciente.telefonopaciente = this.data.telefonopaciente;
      this.paciente.ocupacionpaciente = this.data.ocupacionpaciente;
      this.paciente.estadocivilpaciente=this.data.estadocivilpaciente;
      this.paciente.niveleducacionpaciente=this.data.niveleducacionpaciente;
      this.paciente.datospadrepaciente=this.data.datospadrepaciente;
      this.paciente.datosmadrepaciente=this.data.datosmadrepaciente;
      this.paciente.tipopaciente=this.data.tipopaciente;

      if(this.data.hcl!= null){
        this.idTipopacienteSeleccionado=this.data.tipopaciente.idtipopaciente;
      }


      // //websocket

      // try {
      //   console.log('entro')
      //   this.notificacion = <Subject<any>>this.wsService
      //   .connect(this.urlSocket+this.idUsuarioSocket)
      //   .pipe(map((response: MessageEvent): any => {
      //     return response;
      //   }));
    
      // this.notificacion.subscribe(msg => {
      //   console.log('Socket: ', JSON.parse(msg.data));
      //   let recibido = JSON.parse(msg.data);
      //   console.log(recibido.hcl);
        // this.hclinterno.hcl=recibido.hcl;
      // });
      // } catch (error) {
      //   console.log('error')
      //   console.log(error)
      // }


    }


    listarTipopaciente() {
      this.tipopacienteService.listarTipoPaciente().subscribe(data => {
        this.tipopacientes=data;
      });
    }

    listarInterno(){
      this.pacienteService.listarInterno().subscribe(data =>{
        this.hclinterno=data;
      })
    }
  
    listarExterno(){
      this.pacienteService.listarExterno().subscribe(data =>{
        this.hclexterno=data;
      })
    }

    operar(){

        if(this.paciente!=null && this.data.idpaciente != null){
          let tipopaciente = new Tipopaciente();
          tipopaciente.idtipopaciente= this.idTipopacienteSeleccionado;
          this.paciente.tipopaciente=tipopaciente;
  
          this.pacienteService.modificarpaciente(this.paciente).subscribe(data => {
            
              this.pacienteService.listarpaciente().subscribe(pacientes => {
                this.pacienteService.pacienteCambio.next(pacientes);
                this.pacienteService.mensaje.next("Se modificó correctamente");
              });
              
            this.dialogRef.close();
          });
  
        }else{

          if (this.paciente.hcl==0) {

          let tipopaciente = new Tipopaciente();
          tipopaciente.idtipopaciente= this.idTipopacienteSeleccionado;
          this.paciente.tipopaciente=tipopaciente;
  
             if (this.paciente.nombresyapellidos!=null
              &&this.paciente.tipopaciente!=null) {          

                this.pacienteService.registrarpaciente(this.paciente).subscribe(data => {
                  this.pacienteService.listarpaciente().subscribe(pacientes => {
                    this.pacienteService.pacienteCambio.next(pacientes);
                    this.pacienteService.mensaje.next("Se registró correctamente");
                  });
                
              });
              this.pacienteService.hclWebsocket(this.idUsuarioSocket,this.paciente.hcl).subscribe(data=>{
              })
              this.dialogRef.close();
              
              }else{this.pacienteService.mensaje.next('Falta algún dato requerido del paciente')}

          }else{

          if (this.paciente.hcl===this.hclinterno.hcl+1) {

          let tipopaciente = new Tipopaciente();
          tipopaciente.idtipopaciente= this.idTipopacienteSeleccionado;
          this.paciente.tipopaciente=tipopaciente;
  
             if (this.paciente.nombresyapellidos!=null
              &&this.paciente.tipopaciente!=null) {

                this.pacienteService.BuscarHCLExistente(this.paciente.hcl,this.paciente.nombresyapellidos).subscribe(data=>{
                  this.verificacion=data;

                  if (this.verificacion==null) {
                    console.log(this.verificacion)
                    this.pacienteService.registrarpaciente(this.paciente).subscribe(data => {
                      this.pacienteService.listarpaciente().subscribe(pacientes => {
                        this.pacienteService.pacienteCambio.next(pacientes);
                        this.pacienteService.mensaje.next("Se registró correctamente");
                      });
                    
                  });
                  this.pacienteService.hclWebsocket(this.idUsuarioSocket,this.paciente.hcl).subscribe(data=>{
                  })
                  this.dialogRef.close();
                  }else{
                    this.pacienteService.mensaje.next('El paciente ya esta registrado')
                  }



                })

               

              
              }else{this.pacienteService.mensaje.next('Falta algún dato requerido del paciente')}

        }else{
          this.pacienteService.mensaje.next('El número de hcl ya fue asignado, porfavor genere nuevamente el hcl')
              }
          }  
      }     
    }
  
    cancelar(){
      this.dialogRef.close();
      this.pacienteService.mensaje.next('Se canceló el procedimiento');
    }

    traerhcl(){
      let n = this.hclinterno.hcl+1;
      console.log(n)
      if (n<10) {
        this.paciente.hcl=n;
      }else{
        this.paciente.hcl=n;

      }
    }


    hclexterno1(event: MatSelectChange) {
      this.estadoexterno=event.source.value;
      console.log(this.estadoexterno)
      if (this.estadoexterno==2) {
        this.paciente.hcl=0;
        console.log(this.paciente);
      }else{
      }

    }

    estadoBotonAgregar() {
      if (this.estadoexterno == 2) {
        return true
      }
      else {
        return false;
      };
    }


}
