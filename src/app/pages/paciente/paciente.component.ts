import { DialogmedicosxdiaComponent } from './dialogmedicosxdia/dialogmedicosxdia.component';
import { HOST_SOCKET } from './../../_shared/var.constant';
import { Dialoghistorial2Component } from './dialoghistorial2/dialoghistorial2.component';
import { PacienteService } from './../../_service/paciente.service';
import { Paciente } from './../../_model/paciente';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogpacienteComponent } from './dialogpaciente/dialogpaciente.component';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { WebsocketService } from 'src/app/_service/websocket.service';
declare function actualizarHcl(hcl);
@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  pacientes: Paciente[] = [];
  displayedColumns = ['hcl', 'dnipaciente', 'nombresyapellidos','tipopaciente','acciones'];
  dataSource: MatTableDataSource<Paciente>;
  mensaje: string;
  row:Paciente;

  idUsuarioSocket: number;
  urlSocket = 'ws://'+HOST_SOCKET+'/client?user_id=';
  public notificacion:Subject<any>;

  pacient:string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private pacienteService:PacienteService,
    public dialog:MatDialog, 
    public snackBar:MatSnackBar,
    private wsService:WebsocketService) { }

ngOnInit() {
  if (sessionStorage.getItem("nombre")=="triajet") {
    this.idUsuarioSocket = 0;
    console.log(this.idUsuarioSocket)
  }else{
    this.idUsuarioSocket = 2;
    console.log(this.idUsuarioSocket)
  }

  try {
        console.log('entro')
        this.notificacion = <Subject<any>>this.wsService
        .connect(this.urlSocket+this.idUsuarioSocket)
        .pipe(map((response: MessageEvent): any => {
          return response;
        }));
    
      this.notificacion.subscribe(msg => {
        console.log('Socket: ', JSON.parse(msg.data));
        let recibido = JSON.parse(msg.data);
        console.log(recibido.hcl);
        // this.hclinterno.hcl=recibido.hcl;
        actualizarHcl(recibido.hcl);
      });
      } catch (error) {
        console.log('error')
        console.log(error)
      }

    this.pacienteService.pacienteCambio.subscribe(data => {
      this.pacientes = data;
      this.dataSource = new MatTableDataSource(this.pacientes);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.pacienteService.mensaje.subscribe(data => {
      
      this.snackBar.open(data, null, { duration: 2000 });
    });


  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(paciente: Paciente): void {
    let pro = paciente != null ? paciente : new Paciente();
    let dialogRef = this.dialog.open(DialogpacienteComponent, {
      width: '600px',   
      disableClose: true,   
      
      data: pro      
    });
  }

  openModal(paciente: Paciente):void{
    let pac = paciente;
    let dialogref = this.dialog.open(Dialoghistorial2Component,{
      width: '900px',   
      disableClose: true,   
      data:pac
    });
  }

  abrir(){
    let dialogRef1 = this.dialog.open(DialogmedicosxdiaComponent, {
      width: '900px',   
      disableClose: false, 
      data: ''  
    });
  }

consultar(){
  if (this.pacient!='') {
    this.pacienteService.buscarPorNombre(this.pacient).subscribe(data=>{
      if (data.length==0) {
        this.pacienteService.mensaje.next('No se encontraron coincidencias');
      }
      this.pacientes=data;
      this.dataSource = new MatTableDataSource(this.pacientes);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }else{
    this.pacienteService.mensaje.next('Ingrese un indicio del nombre a buscar')
  }
}

}