import { ComprobantepagoService } from './../../../_service/comprobantepago.service';
import { HistorialService } from './../../../_service/historial.service';
import { Medicoxdia } from './../../../_model/medicoxdia';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-dialogmedicosxdia',
  templateUrl: './dialogmedicosxdia.component.html',
  styleUrls: ['./dialogmedicosxdia.component.css']
})
export class DialogmedicosxdiaComponent implements OnInit {

  at:any='';
  fechaSeleccionada3: Date = new Date();
  maxFecha: Date = new Date();
  constructor(public dialog:MatDialog, 
    public snackBar:MatSnackBar,
    private comprobantepagoService:ComprobantepagoService) { }

  ngOnInit() {
  }

  pdfAtencion(){
      if (this.fechaSeleccionada3!=null) {
          let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAtencion(s1).subscribe(data=>{
            this.at=data;
          })
      }else{
        this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
      }
    }

}
