import { startWith, map } from 'rxjs/operators';
import { ServiciomedicoService } from 'src/app/_service/serviciomedico.service';
import { PersonalmedicoService } from './../../../_service/personalmedico.service';
import { HistorialService } from './../../../_service/historial.service';
import { PacienteService } from './../../../_service/paciente.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatAutocompleteSelectedEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { Serviciomedico } from './../../../_model/serviciomedico';
import { Personalmedico } from './../../../_model/personalmedico';
import { Historial } from './../../../_model/historial';
import { Component, OnInit, Inject } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import * as moment from 'moment';
import { toDate } from '@angular/common/src/i18n/format_date';

@Component({
  selector: 'app-dialoghistorial2',
  templateUrl: './dialoghistorial2.component.html',
  styleUrls: ['./dialoghistorial2.component.css']
})
export class Dialoghistorial2Component implements OnInit {
  paciente= new Paciente();
  historial = new Historial();
  personalmedico = new Personalmedico();

  personalmedicos: Personalmedico[] = [];
  serviciosmedicos: Serviciomedico[] = [];

  personal = new FormControl();

  form:FormGroup;
  form1:FormGroup;
  filteredOptions1: Observable<Personalmedico[]>;

  personalseleccionado: Personalmedico;
  serviciomedicoseleccionado: string;

  idpersonalseleccionado: string;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  age;
  showAge;

  h2:Historial;
  hist:any='';
  
  constructor(public dialogRef: MatDialogRef<Dialoghistorial2Component>,
    @Inject(MAT_DIALOG_DATA) public data: Paciente,
    private pacienteService: PacienteService,
    private historialService: HistorialService,
    private personalmedicoService: PersonalmedicoService,
    private serviciomedicoService: ServiciomedicoService) {
      this.form1=new FormGroup({
        'personal': new FormControl(null),
      });
  }

  ngOnInit() {
    this.listarPersonal();
    this.listarservicio();
    this.ageCalculator();

    this.historial = new Historial();
    this.historial.idhistoriaclinica;
    this.historial.anamnesis;
    this.historial.diagnostico;
    this.historial.edad=this.showAge;
    this.historial.evaluacion; 
    this.historial.examenes;
    this.historial.fechaexpediciconhistoriaclinica=this.fechaSeleccionada;
    this.historial.serviciomedico;
    this.historial.tratamiento;
    this.historial.reconsulta;
    this.historial.tratamientor;
    this.historial.diagnosticor;
    this.historial.personalmedico;
    this.historial.estado=0;

    this.filteredOptions1 = this.personal.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombrespersonalmedico),
      map(nombrespersonalmedico => nombrespersonalmedico ? this._filter1(nombrespersonalmedico) : this.personalmedicos.slice())
    );

  }

  
  listarservicio() {
    this.serviciomedicoService.listarServicioMedico().subscribe(data => {
      this.serviciosmedicos = data;
    });
  }

  listarPersonal() {
    this.personalmedicoService.listarPersonalMedico().subscribe(data => {
      this.personalmedicos = data;
    });
  }

  
  displayFn1(personalmedico?: Personalmedico): string | undefined {
    return personalmedico ? personalmedico.nombrespersonalmedico : undefined;
  }

  private _filter1(nombrespersonalmedico: string): Personalmedico[] {
    const filterValue = nombrespersonalmedico.toLowerCase();
    return this.personalmedicos.filter(personal => personal.nombrespersonalmedico.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
    this.personalseleccionado = event.option.value;
    console.log(this.personalseleccionado)
  }


    // let s;
    // let a= moment(this.data.fechanacimientopaciente, "YYYY-MM-DD",true);
    // s = moment(this.data.fechanacimientopaciente,"YYYY-MM-DD",true).startOf('day').fromNow();
    // console.log(s,a);
    ageCalculator(){
      if(this.data.fechanacimientopaciente){
        let convertAge = new Date(this.data.fechanacimientopaciente);
        let timeDiff = Math.abs(Date.now() - convertAge.getTime());
        this.showAge = Math.floor((timeDiff / (1000 * 3600 * 24))/365);
      }
    }

    operar(){
      let paciente = new Paciente();
      paciente.idpaciente=this.data.idpaciente;
      this.historial.paciente = paciente;

      if(this.historial.edad >0 && this.historial.fechaexpediciconhistoriaclinica != null && 
        this.personalseleccionado !=null && this.serviciomedicoseleccionado!=null){
          let ps = new Personalmedico();
          ps.dnipersonalmedico=this.personalseleccionado.dnipersonalmedico;
          this.historial.personalmedico = ps;
          this.historial.serviciomedico=this.serviciomedicoseleccionado;
          
          this.historialService.registrarHistorial(this.historial).subscribe(data => {
              this.pacienteService.mensaje.next("Se registró correctamente");
              
              this.h2=<Historial>data;
              console.log(this.h2);

              this.historialService.generacionHistoria(this.h2.idhistoriaclinica).subscribe(data=>{
                var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
                var fileURL = window.URL.createObjectURL(data2);
                window.open(fileURL,"_blank");
              });
          });

      }else{
        this.pacienteService.mensaje.next('Falta algún dato requerido');
        console.log(this.historial);
      }
    }

  cancelar() {
    this.dialogRef.close();
    this.historialService.mensaje.next('se canceló el procedimiento');
  }

}
