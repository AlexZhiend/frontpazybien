import { ComprobantepagoService } from './../../../_service/comprobantepago.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-dialogatenciones',
  templateUrl: './dialogatenciones.component.html',
  styleUrls: ['./dialogatenciones.component.css']
})
export class DialogatencionesComponent implements OnInit {

  at:any='';
  fechaSeleccionada3: Date = new Date();
  maxFecha: Date = new Date();

  constructor(public dialog:MatDialog, 
    public snackBar:MatSnackBar,
    private comprobantepagoService:ComprobantepagoService) { }

  ngOnInit() {
  }

  pdfAtencion(){
    if (this.fechaSeleccionada3!=null) {
        let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteAtencion(s1).subscribe(data=>{
          this.at=data;
        })
    }else{
      this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
    }
  }

}
