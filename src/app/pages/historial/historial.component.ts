import { DialogatencionesComponent } from './dialogatenciones/dialogatenciones.component';
import { Dialoghistorial3Component } from './dialoghistorial3/dialoghistorial3.component';
import { ComprobantepagoService } from './../../_service/comprobantepago.service';
import { DialoghistorialComponent } from './dialoghistorial/dialoghistorial.component';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { HistorialService } from './../../_service/historial.service';
import { PacienteService } from './../../_service/paciente.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Historial } from 'src/app/_model/historial';
import { MatTableFilter } from 'mat-table-filter';
import { Paciente } from 'src/app/_model/paciente';
import * as moment from 'moment';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  historias: Historial[] = [];
  displayedColumns = ['hcl', 'dnipaciente', 'nombresyapellidos','fechadeexpedicion','acciones'];
  dataSource: MatTableDataSource<Historial>;
  mensaje: string;
  his:any='';
  hist:Historial;
  filterEntity: Historial;
  filterType: MatTableFilter;
  historiaselect= new Historial();
  nombres:String='';
  row:Historial;

  pacient:string;
  fechainicio: Date = new Date();
  fechafin: Date = new Date();
  hcl:number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private pacienteService:PacienteService,
              private historialService:HistorialService,
              private comprobantepagoService:ComprobantepagoService,    
              public dialog:MatDialog,
              public dialog1:MatDialog,
              public dialog2:MatDialog, 
              public snackBar:MatSnackBar
              ) {

   }

  ngOnInit() {
    this.historialService.historialCambio.subscribe(data => {
      this.historias = data;
      this.dataSource = new MatTableDataSource(this.historias);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.listarPorFecha();

    this.historialService.mensaje.subscribe(data => {
      console.log(data);
      this.snackBar.open(data, null, { duration: 2000 });
    });

    // this.historialService.listarHistorial().subscribe(data => {
    //   this.historias = data;
    //   this.dataSource = new MatTableDataSource(this.historias);
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sort = this.sort;
    // });

          
    this.filterEntity = new Historial();
    this.filterEntity.paciente = new Paciente();
    this.filterType = MatTableFilter.ANYWHERE;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  openDialog(historial: Historial): void {
    let pro = historial != null ? historial : new Historial();
    let dialogRef = this.dialog.open(DialoghistorialComponent, {
      width: '700px',   
      disableClose: true,   
      
      data: pro      
    });
  }

  pdf(h:Historial){
      console.log(h.idhistoriaclinica);
        this.historialService.generacionHistoriaVacia(h.idhistoriaclinica).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
          console.log(data)
        });

        // this.comprobantepagoService.reporteComprobanteU(2).subscribe(data=>{
        //   this.his=data;
        // })
  }
  abrir(){
    let dialogRef1 = this.dialog1.open(Dialoghistorial3Component, {
      width: '800px',   
      disableClose: false, 
      data: ''  
    });
  }

  seleccionar(row:Historial){
    this.historiaselect=row;
    this.nombres=this.historiaselect.paciente.nombresyapellidos;
    console.log(this.historiaselect)
  }

  eliminarhist(){
    if (this.historiaselect!=null && this.historiaselect.idhistoriaclinica>0) {
      console.log(this.historiaselect);
      this.historialService.eliminarCP(this.historiaselect.idhistoriaclinica).subscribe(data=>{
        this.historialService.mensaje.next('La historia se eliminó correctamente');
        this.historialService.listarHistorial().subscribe(data =>{
          this.historias = data;
          this.dataSource = new MatTableDataSource(this.historias);
        });
      });

    }else{
      this.historialService.mensaje.next('Error al eliminar la historia')
    }

  }

  atender(row:Historial){
    let historia = new Historial();
    historia=row;
    historia.estado=1;
    console.log(historia)
    this.historialService.modificarHistorial(historia).subscribe(data=>{
      this.historialService.mensaje.next('Se realizó la atención correctamente')
    })

  }

  listarPorFecha(){
    if (this.fechainicio!=null && this.fechafin!=null 
      && this.fechainicio<=this.fechafin) {
        let s1 = moment(this.fechainicio).format("DD-MM-YYYY");
        let s2 = moment(this.fechafin).format("DD-MM-YYYY");
        this.historialService.listarHistoriaPorFecha(s1,s2).subscribe(data=>{
          if (data.length==0) {
            this.historialService.mensaje.next('No se encontraron atenciones para la fecha seleccionada');
          }
          this.historias = data;
          this.dataSource = new MatTableDataSource(this.historias);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        })
    }else{
      this.historialService.mensaje.next('La fecha inicial debe ser menor a la fecha final');
    }

  }

  abrirat(){
    let dialogRef1 = this.dialog2.open(DialogatencionesComponent, {
      width: '900px',   
      disableClose: false, 
      data: ''  
    });
  }


  consultar(){
    if (this.pacient!='') {
      this.historialService.listarHistoriaPorPaciente(this.pacient).subscribe(data=>{
        if (data.length==0) {
          this.pacienteService.mensaje.next('No se encontraron coincidencias');
        }
        this.historias=data;
        this.dataSource = new MatTableDataSource(this.historias);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    }else{
      this.pacienteService.mensaje.next('Ingrese un indicio del nombre a buscar')
    }
  }


  consultarhcl(){
    if (this.hcl!=0) {
      this.historialService.listarHistoriaPorHCL(this.hcl).subscribe(data=>{
        if (data.length==0) {
          this.pacienteService.mensaje.next('No se encontraron coincidencias');
        }
        this.historias=data;
        this.dataSource = new MatTableDataSource(this.historias);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    }else{
      this.pacienteService.mensaje.next('Ingrese el hcl del paciente a buscar')
    }
  }
}