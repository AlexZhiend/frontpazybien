import { PersonalmedicoService } from './../../../_service/personalmedico.service';
import { HistorialService } from '../../../_service/historial.service';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { Medicoreconsulta } from '../../../_model/medicoreconsulta';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { Personalmedico } from 'src/app/_model/personalmedico';
import * as FileSaver from 'file-saver';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-dialoghistorial3',
  templateUrl: './dialoghistorial3.component.html',
  styleUrls: ['./dialoghistorial3.component.css']
})
export class Dialoghistorial3Component implements OnInit {

  medicoreconsultas: Personalmedico[] = [];

  fechaSeleccionadarec: Date = new Date();
  fechaSeleccionadarec2:Date = new Date();

  maxFecha: Date = new Date();
  recSeleccionada: Personalmedico;

  constructor(private historialService:HistorialService,
    public dialog:MatDialog, 
    public snackBar:MatSnackBar,
    private personalmedicoService:PersonalmedicoService) { }

  ngOnInit() {
    this.listarPersonal();
  }

  listarPersonal() {
    this.personalmedicoService.listarSoloMedicos().subscribe(data => {
      this.medicoreconsultas = data;
    });
  }

  // listar(){
  // let s1 = moment(this.fechaSeleccionada).format("DD-MM-YYYY");
  // this.historialService.listarMedicoreconsulta(s1).subscribe(data => {
  //   this.medicoreconsultas = data;
  //   this.dataSource = new MatTableDataSource(this.medicoreconsultas);
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }); 
  // }

  pdfReconsultas(){
    if (this.fechaSeleccionadarec!=null && this.fechaSeleccionadarec2!=null 
      && this.fechaSeleccionadarec<=this.fechaSeleccionadarec2) {

        if (this.recSeleccionada!=null) {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          let dni=this.recSeleccionada.nombrespersonalmedico;
          this.historialService.pdfMedicoreconsulta2(s1,s2,dni).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.recSeleccionada=null;
          })
        } else {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          this.historialService.pdfMedicoreconsulta1(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.recSeleccionada=null;
          })
        }

    }else{
      this.historialService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  } 

  XLSXReconsultas(){
    if (this.fechaSeleccionadarec!=null && this.fechaSeleccionadarec2!=null 
      && this.fechaSeleccionadarec<=this.fechaSeleccionadarec2) {

        if (this.recSeleccionada!=null) {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          let dni=this.recSeleccionada.nombrespersonalmedico;
          this.historialService.XLSXMedicoreconsulta2(s1,s2,dni).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionECORADXMedico.xlsx");
            this.recSeleccionada=null;
          })
        }else {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          this.historialService.XLSXMedicoreconsulta1(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionECORAD.xlsx");
            this.recSeleccionada=null;
          })
        }

    }else{
      this.historialService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }
  
}
