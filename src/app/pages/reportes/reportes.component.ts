import { RecaudoService } from './../../_service/recaudo.service';
import { PersonalmedicoService } from './../../_service/personalmedico.service';
import { Personalmedico } from './../../_model/personalmedico';
import { HistorialService } from './../../_service/historial.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Medicoreconsulta } from './../../_model/medicoreconsulta';
import { ServiciomedicoService } from './../../_service/serviciomedico.service';
import { Serviciomedico } from './../../_model/serviciomedico';
import { ProductoService } from './../../_service/producto.service';
import { ExamenmedicoService } from './../../_service/examenmedico.service';
import { ComprobantepagoService } from './../../_service/comprobantepago.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  fechaSeleccionada1=new Date();
  fechaSeleccionada2=new Date();
  fechaDetallada=new Date();
  fechaSeleccionada3= new Date();
  fechaSeleccionada4=new Date();
  fechaSeleccionada5=new Date();
  fechaSeleccionada6=new Date();
  fechaSeleccionada7=new Date();
  fechaSeleccionada8=new Date();
  fechaSeleccionada9=new Date();
  fechaSeleccionadarec=new Date();
  fechaSeleccionadarec2=new Date();
  fechaecorad1=new Date();
  fechaecorad2=new Date();
  fecharecinicio: Date = new Date();
  fecharecfin: Date = new Date();

  
  vent:any='';
  at:any='';
  rep:any='';
  pro:any='';
  farm:any='';

  servicios: Serviciomedico[]=[];
  ecoyrads: Personalmedico[]=[];
  servicioSeleccionado:string;
  medicoseleccionado:string;
  recSeleccionada= new Personalmedico();

  especialistaseleccionado=new Personalmedico();

  medicoreconsultas: Personalmedico[] = [];
  displayedColumns = ['reconsulta', 'total', 'fechaexpediciconhistoriaclinica'];
  dataSource: MatTableDataSource<Medicoreconsulta>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  blob= new Blob();

  constructor(private comprobantepagoService:ComprobantepagoService,
    private examenmedicoService:ExamenmedicoService,private productoService:ProductoService,
    private serviciomedicoService:ServiciomedicoService,private historialService:HistorialService,
    private personalmedicoService:PersonalmedicoService,
    private recaudoService:RecaudoService) { }

  ngOnInit() {
    this.listarServ();
    this.listarecosrads();
    this.listarPersonal();
  }

  listarecosrads(){
    this.personalmedicoService.listarSoloEcoYRad().subscribe(data=>{
      this.ecoyrads=data;
    })
  }

  testPdf(){
    this.productoService.reporteProductoGeneral().subscribe(data=>{
      this.blob=data;
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    })
  }

  
  listarServ(){
    this.serviciomedicoService.listarServicioMedico().subscribe(data=>{
      this.servicios=data;
    })
  }
  
  listarPersonal() {
    this.personalmedicoService.listarSoloMedicos().subscribe(data => {
      this.medicoreconsultas = data;
    });
  }

  pdfCaja(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<=this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteCaja(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfAtencion(){
    if (this.fechaSeleccionada3!=null) {
        let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteAtencion(s1).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
    }
  }

  pdfDetallado(){
    if (this.fechaDetallada!=null) {
      let s1 = moment(this.fechaDetallada).format("DD-MM-YYYY");
      this.comprobantepagoService.reporteCajaDetallado(s1).subscribe(data=>{
        var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
        var fileURL = window.URL.createObjectURL(data2);
        window.open(fileURL,"_blank");
      })
  }else{
    this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
  }
  }

  pdflab(){
    if (this.fechaSeleccionada4!=null && this.fechaSeleccionada5!=null 
      && this.fechaSeleccionada4<=this.fechaSeleccionada5) {
        let s1 = moment(this.fechaSeleccionada4).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada5).format("DD-MM-YYYY");
        this.examenmedicoService.reporteLaboratorio(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.examenmedicoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }


  generatepdf(){
    this.productoService.reporteProductoGeneral().subscribe(data=>{
      var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
      var fileURL = window.URL.createObjectURL(data2);
      window.open(fileURL,"_blank");
    })
  }


  pdfventas() {
    if (this.fechaSeleccionada6!=null && this.fechaSeleccionada7!=null 
      && this.fechaSeleccionada6<=this.fechaSeleccionada7) {
        let s1 = moment(this.fechaSeleccionada6).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada7).format("DD-MM-YYYY");
        this.productoService.reporteProductoVentas(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.productoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
    
  }


  //Reportes en excel
  
  pdfCajaXLSX(){
    if (this.fechaSeleccionada1!=null && this.fechaSeleccionada2!=null 
      && this.fechaSeleccionada1<=this.fechaSeleccionada2) {
        let s1 = moment(this.fechaSeleccionada1).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada2).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteCajaXLSX(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"ReporteCajaGeneral.xlsx");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfAtencionXLSX(){
    if (this.fechaSeleccionada3!=null) {
        let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        this.comprobantepagoService.reporteAtencionXLSX(s1).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"Atenciones-"+s1+".xlsx");
        })
    }else{
      this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
    }
  }

  pdfDetalladoXLSX(){
    if (this.fechaDetallada!=null) {
      let s1 = moment(this.fechaDetallada).format("DD-MM-YYYY");
      this.comprobantepagoService.reporteCajaDetalladoXLSX(s1).subscribe(data=>{
        var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
        FileSaver.saveAs(data2,"RDetalladoCaja-"+s1+".xlsx");
      })
  }else{
    this.comprobantepagoService.mensaje.next('Seleccione la fecha a consultar');
  }
  }

  pdfLabxlsx(){
    if (this.fechaSeleccionada4!=null && this.fechaSeleccionada5!=null 
      && this.fechaSeleccionada4<=this.fechaSeleccionada5) {
        let s1 = moment(this.fechaSeleccionada4).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada5).format("DD-MM-YYYY");
        this.examenmedicoService.reporteLaboratorioXLSX(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"LabGeneral"+s1+"-"+s2+".xlsx");
        })
    }else{
      this.examenmedicoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  excelventas() {
    if (this.fechaSeleccionada6!=null && this.fechaSeleccionada7!=null 
      && this.fechaSeleccionada6<=this.fechaSeleccionada7) {
        let s1 = moment(this.fechaSeleccionada6).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada7).format("DD-MM-YYYY");
        this.productoService.reporteProductoVentasXls(s1,s2).subscribe(data=>{
          var data1 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data1,"VentasFarmacia.xlsx");  
        })
    }else{
      this.productoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  generateexcelpdf(){
    this.productoService.reporteProductoXls().subscribe(data=>{
      var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
      FileSaver.saveAs(data2,"StockProductos.xlsx");
    })
  }



  pdfAtencionesG(){
    if (this.fechaSeleccionada8!=null && this.fechaSeleccionada9!=null 
      && this.fechaSeleccionada8<=this.fechaSeleccionada9) {
        console.log(this.servicioSeleccionado+'---'+this.medicoseleccionado);

        if (this.servicioSeleccionado!=null) {
          let s1 = moment(this.fechaSeleccionada8).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionada9).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAteniconesGServ(s1,s2,this.servicioSeleccionado).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.servicioSeleccionado=null;
            this.medicoseleccionado=null;
          })
        }else if(this.medicoseleccionado!=null) {
          let s1 = moment(this.fechaSeleccionada8).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionada9).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAteniconesGMed(s1,s2,this.medicoseleccionado).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.servicioSeleccionado=null;
            this.medicoseleccionado=null;
          })
        } else {
          let s1 = moment(this.fechaSeleccionada8).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionada9).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAteniconesG(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.servicioSeleccionado=null;
            this.medicoseleccionado=null;
          })
        }

    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  pdfAtencionesGXLSX(){

    if (this.fechaSeleccionada8!=null && this.fechaSeleccionada9!=null 
      && this.fechaSeleccionada8<=this.fechaSeleccionada9) {
        console.log(this.servicioSeleccionado+'---'+this.medicoseleccionado);

        if (this.servicioSeleccionado!=null) {
          let s1 = moment(this.fechaSeleccionada8).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionada9).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAteniconesGXLSXServ(s1,s2,this.servicioSeleccionado).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionXServicio.xlsx");
            this.servicioSeleccionado=null;
            this.medicoseleccionado=null;
          })
        }else if(this.medicoseleccionado!=null) {
          let s1 = moment(this.fechaSeleccionada8).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionada9).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAteniconesGXLSXMed(s1,s2,this.medicoseleccionado).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionXMedico.xlsx");
            this.servicioSeleccionado=null;
            this.medicoseleccionado=null;
          })
        } else {
          let s1 = moment(this.fechaSeleccionada8).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionada9).format("DD-MM-YYYY");
          this.comprobantepagoService.reporteAteniconesGXLSX(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionGeneral.xlsx");
            this.servicioSeleccionado=null;
            this.medicoseleccionado=null;
          })
        }

    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  //reportes de Ecografistas y radiologos

  pdfAtencionesEcoRad(){
    if (this.fechaecorad1!=null && this.fechaecorad2!=null 
      && this.fechaecorad1<=this.fechaecorad2) {

        if (this.especialistaseleccionado!=null) {
          let s1 = moment(this.fechaecorad1).format("DD-MM-YYYY");
          let s2 = moment(this.fechaecorad2).format("DD-MM-YYYY");
          let dni=this.especialistaseleccionado.dnipersonalmedico;
          this.personalmedicoService.reporteAteniconesEcoRad2(s1,s2,dni).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.especialistaseleccionado=null;
          })
        } else {
          let s1 = moment(this.fechaecorad1).format("DD-MM-YYYY");
          let s2 = moment(this.fechaecorad2).format("DD-MM-YYYY");
          this.personalmedicoService.reporteAteniconesEcoRad(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.especialistaseleccionado=null;
          })
        }

    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  AtencionesEcoRadXLSX(){
    if (this.fechaecorad1!=null && this.fechaecorad2!=null 
      && this.fechaecorad1<=this.fechaecorad2) {

        if (this.especialistaseleccionado!=null) {
          let s1 = moment(this.fechaecorad1).format("DD-MM-YYYY");
          let s2 = moment(this.fechaecorad2).format("DD-MM-YYYY");
          let dni=this.especialistaseleccionado.dnipersonalmedico;
          this.personalmedicoService.reporteAteniconesEcoRad2XLSX(s1,s2,dni).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionECORADXMedico.xlsx");
            this.especialistaseleccionado=null;
          })
        }else {
          let s1 = moment(this.fechaecorad1).format("DD-MM-YYYY");
          let s2 = moment(this.fechaecorad2).format("DD-MM-YYYY");
          this.personalmedicoService.reporteAteniconesEcoRadXLSX(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionECORAD.xlsx");
            this.especialistaseleccionado=null;
          })
        }

    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }







  pdfReconsultas(){
    if (this.fechaSeleccionadarec!=null && this.fechaSeleccionadarec2!=null 
      && this.fechaSeleccionadarec<=this.fechaSeleccionadarec2) {

        if (this.recSeleccionada!=null) {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          let dni=this.recSeleccionada.nombrespersonalmedico;
          this.historialService.pdfMedicoreconsulta2(s1,s2,dni).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.recSeleccionada=null;
          })
        } else {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          this.historialService.pdfMedicoreconsulta1(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
            var fileURL = window.URL.createObjectURL(data2);
            window.open(fileURL,"_blank");
            this.recSeleccionada=null;
          })
        }

    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  } 

  XLSXReconsultas(){
    if (this.fechaSeleccionadarec!=null && this.fechaSeleccionadarec2!=null 
      && this.fechaSeleccionadarec<=this.fechaSeleccionadarec2) {

        if (this.recSeleccionada!=null) {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          let dni=this.recSeleccionada.nombrespersonalmedico;
          this.historialService.XLSXMedicoreconsulta2(s1,s2,dni).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionECORADXMedico.xlsx");
            this.recSeleccionada=null;
          })
        }else {
          let s1 = moment(this.fechaSeleccionadarec).format("DD-MM-YYYY");
          let s2 = moment(this.fechaSeleccionadarec2).format("DD-MM-YYYY");
          this.historialService.XLSXMedicoreconsulta1(s1,s2).subscribe(data=>{
            var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
            FileSaver.saveAs(data2,"RAtencionECORAD.xlsx");
            this.recSeleccionada=null;
          })
        }

    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }


  recaudoreport(){
    if (this.fecharecinicio!=null && this.fecharecfin!=null 
      && this.fecharecinicio<=this.fecharecfin) {
        let s1 = moment(this.fecharecinicio).format("DD-MM-YYYY");
        let s2 = moment(this.fecharecfin).format("DD-MM-YYYY");
        this.recaudoService.ventasingresosXLSX(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/vnd.ms-excel;charset=utf-8'});
          FileSaver.saveAs(data2,"Reporte.xlsx");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

  recaudoreportpdf(){
    if (this.fecharecinicio!=null && this.fecharecfin!=null 
      && this.fecharecinicio<=this.fecharecfin) {
        let s1 = moment(this.fecharecinicio).format("DD-MM-YYYY");
        let s2 = moment(this.fecharecfin).format("DD-MM-YYYY");
        this.recaudoService.ventasingresosPDF(s1,s2).subscribe(data=>{
          var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
          var fileURL = window.URL.createObjectURL(data2);
          window.open(fileURL,"_blank");
        })
    }else{
      this.comprobantepagoService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

}
