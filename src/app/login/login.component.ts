import { WebsocketAPI } from './../WebSocketAPI';
import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/_service/login.service';
import { Router } from '@angular/router';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';
import { MenuService } from 'src/app/_service/menu.service';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  clave: string;

  webSocketAPI: WebsocketAPI;
  greeting: any;
  name: string;

  constructor(private loginService: LoginService, private router: Router,  private menuService: MenuService) { }

  ngOnInit() {
    // this.webSocketAPI = new WebsocketAPI(new LoginComponent(this.loginService,this.router,this.menuService));
  }



  iniciarSesion(){
    this.loginService.login(this.usuario, this.clave).subscribe(data=>{
      //console.log(data);
      if (data) {
        let token = JSON.stringify(data);
        sessionStorage.setItem(TOKEN_NAME, token);

        let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        const decodedToken = decode(tk.access_token);
        
        this.menuService.listarPorUsuario(decodedToken.user_name).subscribe(data => {
          this.menuService.menuCambio.next(data);
        });
        sessionStorage.setItem("nombre",this.usuario);
        
        let roles = decodedToken.authorities;
        for (let i = 0; roles.length; i++) {
          let rol = roles[i];
          if (rol === 'Admision') {
            this.router.navigate(['historial']);
            break;  
          } else {
            if (rol === 'Farmacia') {
              this.router.navigate(['producto']);
              break;  
            }else{
              if (rol === 'Laboratorio') {
                this.router.navigate(['detalleexamengeneral']);
                break;  
              }else{
                if (rol === 'Caja') {
                  this.router.navigate(['comprobantepago']);
                  break;  
                }else{
                  if (rol === 'TRIAJE') {
                    this.router.navigate(['paciente']);
                    break;  
                  }else{
                    if (rol === 'Admin') {
                      this.router.navigate(['reportes']);
                      break;  
                    }else{
                        this.router.navigate(['paciente']);
                        break;
                    }
                  }  
                }
              }
            }
          }
        }
      }

    });


  }

  // connect(){
  //   this.webSocketAPI._connect();
  // }

  // disconnect(){
  //   this.webSocketAPI._disconnect();
  // }

  // sendMessage(){
  //   this.webSocketAPI._send(this.name);
  // }

  // handleMessage(message){
  //   this.greeting = message;
  // }



}
