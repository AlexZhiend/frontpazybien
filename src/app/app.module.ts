import { Dialoghistorial3Component } from './pages/historial/dialoghistorial3/dialoghistorial3.component';
import { Dialoghistorial2Component } from './pages/paciente/dialoghistorial2/dialoghistorial2.component';
import { ServerErrorsInterceptor } from './_shared/server-errors.interceptor';
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriaexamenmedicoComponent } from './pages/categoriaexamenmedico/categoriaexamenmedico.component';
import { CategoriaproductoComponent } from './pages/categoriaproducto/categoriaproducto.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { PresentacionComponent } from './pages/presentacion/presentacion.component';
import { ProveedorComponent } from './pages/proveedor/proveedor.component';
import { ServiciomedicoComponent } from './pages/serviciomedico/serviciomedico.component';
import { TipopacienteComponent } from './pages/tipopaciente/tipopaciente.component';
import { TipopersonalmedicoComponent } from './pages/tipopersonalmedico/tipopersonalmedico.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriaexamenmedicoEdicionComponent } from './pages/categoriaexamenmedico/categoriaexamenmedico-edicion/categoriaexamenmedico-edicion.component';
import { CategoriaproductoEdicionComponent } from './pages/categoriaproducto/categoriaproducto-edicion/categoriaproducto-edicion.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { PresentacionEdicionComponent } from './pages/presentacion/presentacion-edicion/presentacion-edicion.component';
import { ProveedorEdicionComponent } from './pages/proveedor/proveedor-edicion/proveedor-edicion.component';
import { ServiciomedicoEdicionComponent } from './pages/serviciomedico/serviciomedico-edicion/serviciomedico-edicion.component';
import { TipopacienteEdicionComponent } from './pages/tipopaciente/tipopaciente-edicion/tipopaciente-edicion.component';
import { TipopersonalmedicoEdicionComponent } from './pages/tipopersonalmedico/tipopersonalmedico-edicion/tipopersonalmedico-edicion.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { ExamenmedicoComponent } from './pages/examenmedico/examenmedico.component';
import { PersonalmedicoComponent } from './pages/personalmedico/personalmedico.component';
import { ComprobantepagoComponent } from './pages/comprobantepago/comprobantepago.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { OrdenComponent } from './pages/orden/orden.component';
import { ExamenesgeneralesComponent } from './pages/examenesgenerales/examenesgenerales.component';
import { DialogproductoComponent } from './pages/producto/dialogproducto/dialogproducto.component';
import { DialogexamenmedicoComponent } from './pages/examenmedico/dialogexamenmedico/dialogexamenmedico.component';
import { DialogpacienteComponent } from './pages/paciente/dialogpaciente/dialogpaciente.component';
import { DialogPersonalmedicoComponent } from './pages/personalmedico/dialog-personalmedico/dialog-personalmedico.component';
import { ExamenesgeneralesEdicionComponent } from './pages/examenesgenerales/examenesgenerales-edicion/examenesgenerales-edicion.component';
import { DetalleegComponent } from './pages/detalleeg/detalleeg.component';
import { HistorialComponent } from './pages/historial/historial.component';
import { DialoghistorialComponent } from './pages/historial/dialoghistorial/dialoghistorial.component';
import { LoginComponent } from './login/login.component';
import { Not403Component } from './pages/not403/not403.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { MatTableFilterModule } from 'mat-table-filter';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule, MAT_MOMENT_DATE_FORMATS} from '@angular/material-moment-adapter';
import { DialogexamenegComponent } from './pages/detalleeg/dialogexameneg/dialogexameneg.component';
import { DialogmedicosxdiaComponent } from './pages/paciente/dialogmedicosxdia/dialogmedicosxdia.component';
import { DialogcomprobanteComponent } from './pages/comprobantepago/dialogcomprobante/dialogcomprobante.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { DialogeliminarComponent } from './pages/comprobantepago/dialogeliminar/dialogeliminar.component';
import { DialogatencionesComponent } from './pages/historial/dialogatenciones/dialogatenciones.component';
import { NotadecreditoComponent } from './pages/comprobantepago/notadecredito/notadecredito.component';
import { NotacreditoOrdenComponent } from './pages/orden/notacredito-orden/notacredito-orden.component';


@NgModule({
  declarations: [
    AppComponent,
    CategoriaexamenmedicoComponent,
    CategoriaproductoComponent,
    EspecialidadComponent,
    PresentacionComponent,
    ProveedorComponent,
    ServiciomedicoComponent,
    TipopacienteComponent,
    TipopersonalmedicoComponent,
    CategoriaexamenmedicoEdicionComponent,
    CategoriaproductoEdicionComponent,
    EspecialidadEdicionComponent,
    PresentacionEdicionComponent,
    ProveedorEdicionComponent,
    ServiciomedicoEdicionComponent,
    TipopacienteEdicionComponent,
    TipopersonalmedicoEdicionComponent,
    PacienteComponent,
    ExamenmedicoComponent,
    PersonalmedicoComponent,
    ComprobantepagoComponent,
    ProductoComponent,
    OrdenComponent,
    ExamenesgeneralesComponent,
    DialogproductoComponent,
    DialogexamenmedicoComponent,
    DialogpacienteComponent,
    DialogPersonalmedicoComponent,
    ExamenesgeneralesEdicionComponent,
    DetalleegComponent,
    HistorialComponent,
    DialoghistorialComponent,
    LoginComponent,
    Not403Component,
    Dialoghistorial2Component,
    DialogexamenegComponent,
    Dialoghistorial3Component,
    DialogmedicosxdiaComponent,
    DialogcomprobanteComponent,
    ReportesComponent,
    DialogeliminarComponent,
    DialogatencionesComponent,
    NotadecreditoComponent,
    NotacreditoOrdenComponent
    
    ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,NgxExtendedPdfViewerModule,MatTableFilterModule,MatMomentDateModule
  ],
  entryComponents:[DialogproductoComponent,DialogexamenmedicoComponent, 
    DialogpacienteComponent, DialogPersonalmedicoComponent,DialoghistorialComponent,
  Dialoghistorial2Component,DialogexamenegComponent,Dialoghistorial3Component,DialogmedicosxdiaComponent,
  DialogcomprobanteComponent,DialogeliminarComponent,DialogatencionesComponent,NotadecreditoComponent,
  NotacreditoOrdenComponent],
  providers: [{provide:LocationStrategy, useClass: HashLocationStrategy},{
    provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true,
  },{provide: MAT_DATE_LOCALE, useValue: 'es-CO'},    
  {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
  {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},],
  bootstrap: [AppComponent] 
})
export class AppModule { }
