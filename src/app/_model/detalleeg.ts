import { Resultados } from './resultados';
import { ExamenMedico } from './examenmedico';

export class DetalleExamen{
    iddetalleexamen:string;
    resultado:string;
    examenmedico: ExamenMedico;
    resultados: Resultados;
}