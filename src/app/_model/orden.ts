import { DetalleOF } from './detalleof';
export class OrdenFarmacia{
    idordenfarmacia:number;
    fechaordenfarmacia:Date;
    rucordenfarmacia:string;
    consumidorordenfarmacia:string;
    detalleordenF:any;
    numeroorden:number;
    total:number;
    descuento:number;
    estado:number;
    serie:number;
    tipo:number;
    serieref:number;
    tiporef:number;
    totalref:number;
    fechaordenref:Date;
    numeroref:number;
}
