import { Categoriaexamenmedico } from './categoriaexamenmedico';

export class ExamenMedico{
    idexamenmedico: number;
    denominacionexamenmedico: string;
    unidadmedida: string;
    valorreferencia: number;
    extra:number;
    categoriaexamenmedico: Categoriaexamenmedico;
}