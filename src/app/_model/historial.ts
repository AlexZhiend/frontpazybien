import { Paciente } from 'src/app/_model/paciente';
import { Personalmedico } from './personalmedico';
export class Historial{
    idhistoriaclinica:number;
    fechaexpediciconhistoriaclinica:Date;
    serviciomedico:string;
    edad:number;
    anamnesis:string;
    evaluacion:string;
    diagnostico:string;
    tratamiento:string;
    examenes:string;
    personalmedico:Personalmedico;
    paciente:Paciente;
    reconsulta:string;
    diagnosticor:string;
    tratamientor:string;
    fechareconsulta:Date;
    estado:number;
}