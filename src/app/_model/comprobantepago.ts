import { DetalleComprobante } from './detallecomprobante';
import { Paciente } from 'src/app/_model/paciente';
import { Serviciomedico } from './serviciomedico';
export class ComprobantePago{
    idcomprobantepago:number;
    fechacomprobante:Date;
    fechaatencion:Date;
    turno:string;
    numerorecibocomprobante:number;
    paciente:Paciente;
    serviciomedico:Serviciomedico;
    detallecomprobante:DetalleComprobante[];
    cantidadfarmacia:number;
    cantidadotros:number;
    cantidadtopico:number;
    total:number;
    descuento:number;
    estado:number;
    fechaatlab:Date;
    serie:number;
    tipo:number;
    serieref:number;
    tiporef:number;
    totalref:number;
    fechacompref:Date;
    numeroref:number;
}