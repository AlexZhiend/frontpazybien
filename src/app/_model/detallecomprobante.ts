import { Categoriaexamenmedico } from 'src/app/_model/categoriaexamenmedico';
import { ComprobantePago } from './comprobantepago';
import { ExamenMedico } from './examenmedico';

export class DetalleComprobante{
    iddetallecomprobante: number;
    cantidad:number
    comprobantepago:ComprobantePago;
    categoriaexamenmedico:Categoriaexamenmedico;
    importe:number;
    dnipersonalmedico:string;
}