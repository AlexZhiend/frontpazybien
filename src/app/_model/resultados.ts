import { DetalleExamen } from './detalleeg';
import { Paciente } from './paciente';

export class Resultados{
    idresultados: number;
    fecha: Date;
    observaciones: string;
    paciente: Paciente;
    detalleexamen:DetalleExamen[];
}



